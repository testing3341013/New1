﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
//using System.Web.Http;
using log4net;
//using LS.Data;
//using LS.Data.Model.CatalogSectionModels;
//using LS.Data.Model.Export;
//using LS.Data.Model.ProductPreview;
//using LS.Data.Model.ProductView;
using System.Reflection;
//using Microsoft.Ajax.Utilities;
using Newtonsoft.Json;
using System.Net.Http;
using System.Net;
//using LS.Data.Utilities;
using System.Collections;
using System.Web;
using System.Xml.Serialization;
//using System.Web.Mvc;
//using System.Web.Script.Serialization;
using System.Xml;
using Newtonsoft.Json.Linq;
//using LS.Data.Model;
using System.Xml.Linq;
//using System.Data.Entity.Core.Objects;
using System.Security.Cryptography;
//using LS.Web.Utility;
//using Kendo.DynamicLinq;
//using Stimulsoft.Report;
using System.Text.RegularExpressions;
using System.Diagnostics;
//using Stimulsoft.Report.Export;
//using System.Web.Mvc.Html;
//using System.Web.UI.WebControls;
//using WebGrease.Css.Extensions;
using Missing = System.Reflection.Missing;
//using LS.Data.Model.Reports;
//using LS.Web.Models;
using System.Web.Script.Serialization;
//using System.Transactions;
//using System.Data.Entity.Validation;
//using System.IO.Path;
//using System.Web;



namespace BatchProcessExport
{
    class Program
    {
       
        Connection connection = new Connection();
        SqlConnection sqlConnection = new SqlConnection();
      //  SqlCommand sqlCommand = new SqlCommand();       
        SqlDataAdapter sqlDataAdapter = new SqlDataAdapter();
        DataTable ExportTable = new DataTable();
        DataTable ExportTableFAM = new DataTable();
        DataTable ExportTableSubProduct = new DataTable();

        private static ILog _logger = LogManager.GetLogger(typeof(Program));


        static void Main(string[] args)
        {
            log4net.Config.BasicConfigurator.Configure();  

            //Manually Pass the arguments.
            //     args = new string[3] { "823005cd-579b-453f-9144-4ab2a49524b8", "3", "true" };
           
            try
            {
                _logger.Info(args.Length.ToString()); // TO Check the information in console Window.s
                _logger.Info("Start");
                Program program = new Program(); // Create an object for a class                
                int customerId = 0;
                int.TryParse(args[1], out customerId);

                bool ExportSchedule = false;
                bool.TryParse(args[2], out ExportSchedule);              

                if(ExportSchedule)
                {
                    _logger.Info("ExportProcessBatchRun");
                    program.ExportProcessBatchRun(args[0], customerId, ExportSchedule);
                }
                else
                {
                    _logger.Info("ExportProcessSchedule");                                     
                    program.ExportProcessSchedule();
                }

                //Save values into xls in [F:\Mariya\Project_Source\Rajesh_Backup_Source\src\LS.Web\Content\BatchExportSheet]
               
               

            }
            catch (Exception ex)
            {

                _logger.Error("Error Main", ex);
            }
        }


        public void Download(DataTable ExportTable, DataTable ExportTableFAM, DataTable ExportTableSubProduct, string batchId, string ExportId)
        {
            Download download = new Download();
            download.ProcessRequest(ExportTable, ExportTableFAM, ExportTableSubProduct, batchId, ExportId);
            _logger.Info("Export Completed");
        }

        public void ExportProcessBatchRun(string batchId, int customerId, bool ExportSchedule)
        {
           
            Program program = new Program();
            string BatchId = batchId;
            string ExportId = string.Empty;
            DataTable GetSingleBatchData = new DataTable();
            try
            {
               
                using (sqlConnection = new SqlConnection(connection.GetConnection()))
                {
                    //sqlConnection.Open();
                    SqlCommand objSqlCommand = new SqlCommand();
                    objSqlCommand.CommandType = CommandType.StoredProcedure;
                    
                    objSqlCommand.Connection = sqlConnection;
                    objSqlCommand.CommandText = "STP_LS_EXPORTBATCHPROCESS";

                    objSqlCommand.Parameters.Add("@OPTION", SqlDbType.VarChar,30).Value = "SELECT";
                    objSqlCommand.Parameters.Add("@BATCH_ID", SqlDbType.VarChar, 200).Value = BatchId;
                    objSqlCommand.CommandTimeout = 0;

                    sqlDataAdapter = new SqlDataAdapter(objSqlCommand);
                                 
                    sqlDataAdapter.Fill(GetSingleBatchData);

                }

                if(GetSingleBatchData.Rows.Count > 0)
                {
                    foreach (DataRow Drpending in GetSingleBatchData.Rows)
                    {
                        batchId = Drpending["BATCH_ID"].ToString();
                        ExportId = Drpending["EXPORTID"].ToString();
                        program.FinalExportBatch(batchId, ExportId);
                    } 
                }

            }
            catch (Exception ex)
            {
                
                throw;
            }

        }

        public void ExportProcessSchedule()
        {
            Program program = new Program();
            DataTable dsAllPendingData = new DataTable();
            string batchId = string.Empty;
            string ExportId = string.Empty;

            using(sqlConnection = new SqlConnection(connection.GetConnection()))
            {
                sqlConnection.Open();
                using (SqlCommand sqlCommand = new SqlCommand("STP_LS_EXPORTBATCHPROCESS", sqlConnection))
                {

                    sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                    sqlCommand.Parameters.Add("@OPTION", SqlDbType.VarChar).Value = "SELECTPENDINGDATA";

                    sqlCommand.CommandTimeout = 0;
                    sqlDataAdapter = new SqlDataAdapter(sqlCommand);

                    sqlDataAdapter.Fill(dsAllPendingData);
                }

                if(dsAllPendingData.Rows.Count > 0)
                {

                    foreach(DataRow Drpending in dsAllPendingData.Rows)
                    {
                        batchId = Drpending["BATCH_ID"].ToString();
                        ExportId = Drpending["EXPORTID"].ToString();
                        program.FinalExportBatch(batchId, ExportId);
                    }         
                }
                else
                {
                    _logger.Info("No Data on the Schedule");
                }
            }
        }

        public void FinalExportBatch(string batchId, string ExportId)
        {
            _logger.Info("FinalExportBatch");
            Program program = new Program(); 
            //Declare the variables
            int catalogid = 0;
            string attrBy = string.Empty;
            string filter = string.Empty;
            string familyFilter = string.Empty;
            string catalogname = string.Empty;
            int familyId = 0;
            string projectName = string.Empty;
            string selcategory_id = string.Empty;
            string selfamily_idd = string.Empty;
            string outputformat = string.Empty;
            string Delimiters = string.Empty;
            string fromdate = string.Empty;
            string todate = string.Empty;
            string exportids = string.Empty;
            string hierarchy = string.Empty;
            string fileName = string.Empty;
            string CustomerName = string.Empty;


            string categoryname = string.Empty;



            DataSet GetAllModelData = new DataSet();
            DataTable EXPORTINFO = new DataTable();
            DataTable FUNCTIONALLOWEDITEM = new DataTable();
            DataTable SELFAMILY_ID = new DataTable();
            DataTable DISLAYATTRIBUTES = new DataTable();
            DataTable model33 = new DataTable();
          
            try
            {  
                //To Strt Export Process

                //To Get All the details

                using(sqlConnection = new SqlConnection(connection.GetConnection()))
                {
                    sqlConnection.Open();
                    SqlCommand objSqlCommand = new SqlCommand();
                    objSqlCommand.CommandType = CommandType.StoredProcedure;

                    objSqlCommand.Connection = sqlConnection;
                    objSqlCommand.CommandText = "STP_LS_EXPORTBATCHPROCESS";

                    objSqlCommand.Parameters.Add("@OPTION", SqlDbType.VarChar, 30).Value = "SELECTBATCH";
                    objSqlCommand.Parameters.Add("@BATCH_ID", SqlDbType.VarChar, 200).Value = batchId;
                    objSqlCommand.Parameters.Add("@EXPORTID", SqlDbType.Int).Value = Convert.ToInt32(ExportId);
                    objSqlCommand.CommandTimeout = 0;

                    sqlDataAdapter = new SqlDataAdapter(objSqlCommand);

                    sqlDataAdapter.Fill(GetAllModelData);
                }

                if(GetAllModelData.Tables[0].Rows.Count > 0)
                {
                    EXPORTINFO = GetAllModelData.Tables[0];
                    FUNCTIONALLOWEDITEM = GetAllModelData.Tables[1];
                    DISLAYATTRIBUTES = GetAllModelData.Tables[2];
                    SELFAMILY_ID = GetAllModelData.Tables[3];


                    foreach (DataRow dr in EXPORTINFO.Rows)
                    {
                        catalogid = Convert.ToInt32(dr["CATALOGID"]);
                         attrBy = dr["ATTRIBUTEBY"].ToString();
                         filter = dr["FILTER"].ToString();
                         familyFilter = dr["FAMILYFILTER"].ToString();
                         catalogname = dr["CATALOGNAME"].ToString();
                         familyId = Convert.ToInt32( dr["FAMILYID"]);
                         projectName = dr["PROJECTNAME"].ToString();
                         selcategory_id = dr["CATEGORYIDLIST"].ToString();
                         selfamily_idd = dr["FAMILYIDLIST"].ToString();
                         outputformat = dr["EXPORT_OUTPUT"].ToString();
                         Delimiters = dr["DELIMITER"].ToString();
                         fromdate = dr["FROMDATE"].ToString();
                         todate   = dr["TODATE"].ToString();
                         exportids = dr["EXPORTIDS"].ToString();
                         hierarchy = dr["HIERARCHY"].ToString();
                        fileName = dr["FILENAME"].ToString();
                        CustomerName = dr["CUSTOMER_NAME"].ToString();

                    }

                    //Test _stat


                    //Convert Date Format _ Start

                    if( string.IsNullOrEmpty(fromdate) != true)
                    {
                        var SplitFromDate = fromdate.Split('/');
                        fromdate = SplitFromDate[1] + "/" + SplitFromDate[0] + "/" + SplitFromDate[2];
                    }

                    if (string.IsNullOrEmpty(todate) != true)
                    {
                        var SplitToDate = todate.Split('/');
                        todate = SplitToDate[1] + "/" + SplitToDate[0] + "/" + SplitToDate[2];
                    }

                    // Convert Date Format _End
                  
                    var model0 = JArray.Parse(JsonConvert.SerializeObject(EXPORTINFO));
                    var model1 = JArray.Parse(JsonConvert.SerializeObject(FUNCTIONALLOWEDITEM));
                    var model2 = JArray.Parse(JsonConvert.SerializeObject(model33));
                    var model3 = JArray.Parse(JsonConvert.SerializeObject(SELFAMILY_ID));
                    var model4 = JArray.Parse(JsonConvert.SerializeObject(DISLAYATTRIBUTES));

                    JArray model = new JArray();
                    model.Add(model0);
                    model.Add(model1);
                    model.Add(model2);
                    model.Add(model3);
                    model.Add(model4);

                    _logger.Info("GetAllModelData");

                   // JArray a = JsonConvert.SerializeObject(SELFAMILY_ID);
                 
                                     // var dislayattributes = model[4].ToList();
                    JavaScriptSerializer js = new JavaScriptSerializer();
                   

                    //End 

                    // Batch Process Start - Home API===========================================================================================================================================
                    #region Batch Process Start
                    
                    try
                    {
                        var family_id_model = "";
                        if(SELFAMILY_ID.Rows.Count > 0)
                        {
                             family_id_model = SELFAMILY_ID.Rows[0]["FAMILYID"].ToString();
                        }else
                        {
                            family_id_model = "";
                        }

                       

                        string selfamily_id = family_id_model;

                       // string selfamily_id = model[3].ToString().Replace("[", "").Replace("]", "");

                        //Test
                        sqlConnection = new SqlConnection(connection.GetConnection());
                        String sql1 = "select ENNABLESUBPRODUCTS from EXPORTBATCHPROCESS where EXPORTID = '" + ExportId + "'";
                        SqlCommand cmd5 = new SqlCommand(sql1, sqlConnection);
                        sqlConnection.Open();
                        var EnableSubProduct = (bool)cmd5.ExecuteScalar();
                        sqlConnection.Close();

                        //End
                        //   bool EnableSubProduct = getEnableSubProduct();

                        var objDataSetFamily = new DataSet();
                        var objcustomExport = new Export();
                        DataTable familyExport = new DataTable();
                        var functionAlloweditem = (model[1]).Select(x => x).ToList();
                        var selectedAttributes = (model[2]).Select(x => x).ToList();
                        string systemAttributes = string.Empty;
                        var datarange = new DataSet();
                        string customAttributes = string.Empty;
                        string sqlConn = ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString;
                        var dispaytable = new DataTable();
                        dispaytable.Columns.Add("ATTRIBUTE_NAME");
                        dispaytable.Columns.Add("DISPLAY_NAME");

                        // var dislayattributes = model[4].ToList();
                        DataTable dislayattributes = DISLAYATTRIBUTES;

                        SaveDisplayAttribute(dislayattributes, projectName);
                        // EnableSubProduct = model.Contains("exporsubproducts");
                        foreach (DataRow item in dislayattributes.Rows)
                        {
                            string attributeName = Convert.ToString(item["ATTRIBUTE_NAME"]);
                            string DISPLAY_NAME = Convert.ToString(item["DISPLAY_NAME"]);

                            dispaytable.Rows.Add(attributeName, DISPLAY_NAME);
                        }
                        foreach (var item in functionAlloweditem)
                        {
                            if (Convert.ToInt32(item["ATTRIBUTE_TYPE"]) == 0 || Convert.ToInt32(item["ATTRIBUTE_TYPE"]) == 2)
                            {
                                systemAttributes += item["ATTRIBUTE_NAME"] + ",";
                            }
                            else
                            {
                                customAttributes += Convert.ToInt32(item["ATTRIBUTE_ID"]) == 0 ? item["ATTRIBUTE_NAME"] + "," : item["ATTRIBUTE_ID"] + ",";
                            }
                        }
                        systemAttributes.Remove(systemAttributes.Length - 1);
                        if (customAttributes.Contains(",") && customAttributes != "PRODUCT_ID,")
                        {
                            customAttributes = customAttributes.Remove(customAttributes.Length - 1);
                        }

                        ///Change_Start

                        sqlConnection = new SqlConnection(connection.GetConnection());
                        String sql = "select CustomerId from Customer_User cu join EXPORTBATCHPROCESS Ex on cu.User_Name = Ex.CUSTOMER_NAME where Ex.CUSTOMER_NAME = '" + CustomerName + "' and  Ex.EXPORTID = '" + ExportId + "' ";
                        SqlCommand cmd = new SqlCommand(sql, sqlConnection);
                        sqlConnection.Open();
                        var customerid = (int)cmd.ExecuteScalar();
                        sqlConnection.Close();


                        ///Change_End
                        ///
                        String Query = "select DisplayIDColumns from Customer_Settings where CustomerId = '" + customerid + "'";

                      

                        SqlCommand cmds = new SqlCommand(Query, sqlConnection);
                        sqlConnection.Open();
                        var customerSetting = (bool)cmds.ExecuteScalar();

                        if (customerSetting == true)
                        {
                            int startcount = customAttributes.IndexOf("PRODUCT_ID", StringComparison.Ordinal);
                            customAttributes = customAttributes.Remove(startcount, 11);
                        }
                        if (customAttributes == string.Empty)
                        {
                            customAttributes = "1";
                        }
                        string[] allcustomAttr = customAttributes.Split(',');
                        int[] allintcustomAttr = Array.ConvertAll(allcustomAttr, int.Parse);
                        var objDataSet = new DataSet();
                        DataTable objDataSetFam = new DataTable();
                        var tempdataset = new DataSet();
                        var attributedataset = new DataSet();
                        Guid objGuid = Guid.NewGuid();
                        string sessionid = Regex.Replace(objGuid.ToString(), "[^a-zA-Z0-9_]+", "");
                        var Row_categories = selcategory_id.ToLower().Split(',').ToList();
                        using (var objSqlConnection = new SqlConnection(sqlConn))
                        {
                            objSqlConnection.Open();
                            string _sqlString = null;
                            if (!string.IsNullOrEmpty(fromdate) && !string.IsNullOrEmpty(todate))
                            {
                                _sqlString =
                                   "declare @frm varchar(100)" +
                                   "declare @to varchar(100)" +
                                   "set @frm = (select cast(CONVERT(varchar, '" + fromdate + "', 101) as date))" +
                                   "set @to = (select cast(CONVERT(varchar, '" + todate + "', 101) as date))" +
                                   "exec('SELECT distinct  TPS.PRODUCT_ID into [##Tempcheck" + sessionid + "] FROM TB_PROD_SPECS TPS JOIN TB_PROD_FAMILY TPF ON TPS.PRODUCT_ID=TPF.PRODUCT_ID AND TPF.PUBLISH2EXPORT=1  WHERE cast(CONVERT(varchar, TPS.MODIFIED_DATE, 101) as date) BETWEEN '''+@frm+'''  AND '''+@to+'''')";
                                SqlCommand _DBCommand11 = new SqlCommand(_sqlString, objSqlConnection);
                                _DBCommand11.ExecuteNonQuery();
                                var objSqlDataAdapterexp = new SqlDataAdapter("select * from [##Tempcheck" + sessionid + "]", objSqlConnection);
                                objSqlDataAdapterexp.Fill(datarange);
                                if (!(datarange.Tables.Count > 0 && datarange.Tables[0].Rows.Count > 0))
                                {
                                    return;
                                }

                            }
                            if (attrBy != "Row")
                            {
                                var selected_categories = selcategory_id.ToLower().Split(',').ToList();
                                foreach (var item in selected_categories)
                                {
                                    categoryname = item.ToString();
                                    SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                                    objSqlCommand.CommandText = "STP_LS_Export";
                                    objSqlCommand.CommandType = CommandType.StoredProcedure;
                                    objSqlCommand.CommandTimeout = 0;
                                    objSqlCommand.Connection = objSqlConnection;
                                    objSqlCommand.Parameters.Add("@CATALOG_ID", SqlDbType.NVarChar, 10).Value = catalogid;
                                    objSqlCommand.Parameters.Add("@SESSID", SqlDbType.NVarChar, 500).Value = sessionid;
                                    objSqlCommand.Parameters.Add("@ATTRIBUTE_IDS", SqlDbType.NVarChar).Value = customAttributes;
                                    objSqlCommand.Parameters.Add("@CATEGORY_ID", SqlDbType.NVarChar).Value = categoryname ?? "All";
                                    var objSqlDataAdapter = new SqlDataAdapter(objSqlCommand);
                                    objSqlDataAdapter.Fill(tempdataset);
                                    objDataSet.Merge(tempdataset);
                                    objSqlConnection.Close();
                                }
                              
                                if (tempdataset.Tables.Count > 2)
                                {
                                    familyExport = tempdataset.Tables[2].Clone();
                                    foreach (DataRow dRow in tempdataset.Tables[2].Rows)
                                    {
                                        familyExport.Rows.Add(dRow.ItemArray);
                                    }
                                    foreach (DataColumn dColumn in familyExport.Columns)
                                    {
                                        if (dColumn.ColumnName.ToUpper() == "FAMILY_ID_1")
                                        {
                                            familyExport.Columns.Remove(dColumn);
                                            break;
                                        }
                                    }
                                    familyExport.Columns["FAMILY_PUBLISH2WEB"].SetOrdinal(familyExport.Columns.IndexOf("SUBFAMILY_ID") + 2);
                                    familyExport.Columns["FAMILY_PUBLISH2PDF"].SetOrdinal(familyExport.Columns.IndexOf("FAMILY_PUBLISH2WEB"));
                                    familyExport.Columns["FAMILY_PUBLISH2EXPORT"].SetOrdinal(familyExport.Columns.IndexOf("FAMILY_PUBLISH2PDF"));
                                    familyExport.Columns["FAMILY_PUBLISH2PRINT"].SetOrdinal(familyExport.Columns.IndexOf("FAMILY_PUBLISH2EXPORT"));
                                    familyExport.Columns["FAMILY_PUBLISH2PORTAL"].SetOrdinal(familyExport.Columns.IndexOf("FAMILY_PUBLISH2PRINT"));
                                    familyExport.Columns["CATEGORY_PUBLISH2WEB"].SetOrdinal(familyExport.Columns.IndexOf("FAMILY_ID"));
                                    familyExport.Columns["CATEGORY_PUBLISH2PDF"].SetOrdinal(familyExport.Columns.IndexOf("CATEGORY_PUBLISH2WEB"));
                                    familyExport.Columns["CATEGORY_PUBLISH2EXPORT"].SetOrdinal(familyExport.Columns.IndexOf("CATEGORY_PUBLISH2PDF"));
                                    familyExport.Columns["CATEGORY_PUBLISH2PRINT"].SetOrdinal(familyExport.Columns.IndexOf("CATEGORY_PUBLISH2EXPORT"));
                                    familyExport.Columns["CATEGORY_PUBLISH2PORTAL"].SetOrdinal(familyExport.Columns.IndexOf("CATEGORY_PUBLISH2PRINT"));


                                    //=============  var itemNoName = _dbcontext.Customers.Join(_dbcontext.Customer_User, c => c.CustomerId, cu => cu.CustomerId, (c, cu) => new { c, cu }).Where(y => y.cu.User_Name == User.Identity.Name).Select(x => x.c.CustomizeItemNo).FirstOrDefault();
                                    sqlConnection.Close();
                                    String query = "select c.CustomizeItemNo from Customers c join Customer_User cu on c.CustomerId = cu.CustomerId where cu.User_Name in ( select User_Name from Customer_User cu join EXPORTBATCHPROCESS Ex on cu.User_Name = Ex.CUSTOMER_NAME where Ex.CUSTOMER_NAME = '" + CustomerName + "' and  Ex.EXPORTID = '" + ExportId + "') ";

                                    SqlCommand cmd1 = new SqlCommand(query, sqlConnection);
                                    sqlConnection.Open();
                                    var itemNoName = Convert.ToString(cmd1.ExecuteScalar());

                                    sqlConnection.Close();

                                    if (string.IsNullOrEmpty(itemNoName))
                                    {
                                        itemNoName = "ITEM#";
                                    }



                                    tempdataset.Tables[0].Columns["PRODUCT_PUBLISH2WEB"].SetOrdinal(tempdataset.Tables[0].Columns.IndexOf("PRODUCT_ID") + 2);
                                    tempdataset.Tables[0].Columns["PRODUCT_PUBLISH2PDF"].SetOrdinal(tempdataset.Tables[0].Columns.IndexOf("PRODUCT_PUBLISH2WEB"));
                                    tempdataset.Tables[0].Columns["PRODUCT_PUBLISH2EXPORT"].SetOrdinal(tempdataset.Tables[0].Columns.IndexOf("PRODUCT_PUBLISH2PDF"));
                                    tempdataset.Tables[0].Columns["PRODUCT_PUBLISH2PRINT"].SetOrdinal(tempdataset.Tables[0].Columns.IndexOf("PRODUCT_PUBLISH2EXPORT"));
                                    tempdataset.Tables[0].Columns["PRODUCT_PUBLISH2PORTAL"].SetOrdinal(tempdataset.Tables[0].Columns.IndexOf("PRODUCT_PUBLISH2PRINT"));
                                }
                                // tempdataset.Tables[0].Columns.Add("Action", typeof(Boolean)).SetOrdinal(0);
                                //  tempdataset.Tables[1].Columns.Add("Action", typeof(Boolean)).SetOrdinal(0);
                                DataTable dt_SP = new DataTable();
                                DataTable dt_SP_sel = new DataTable();
                                //if (exporsubproducts == "Yes")
                                if (EnableSubProduct == true)
                                {
                                    dt_SP = objDataSet.Tables[1].Copy();
                                }
                                string ssid = Guid.NewGuid().ToString();
                                string ssidsp = Guid.NewGuid().ToString();
                                string ssidspf = Guid.NewGuid().ToString();
                                _sqlString = "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''[##FAMILYEXPTEMPFAM" + ssid + "]'') BEGIN DROP TABLE [##FAMILYEXPTEMPFAM" + ssid + "] END')";
                                SqlCommand _DBCommand = new SqlCommand(_sqlString, objSqlConnection);
                                //sqlConnection.Close();
                                objSqlConnection.Open();
                                _DBCommand.ExecuteNonQuery();
                                _sqlString = CreateTable("[##FAMILYEXPTEMPFAM" + ssid + "]", familyExport);
                                SqlCommand _DBCommandn = new SqlCommand(_sqlString, objSqlConnection);
                                _DBCommandn.ExecuteNonQuery();
                                var bulkCopy_cp = new SqlBulkCopy(objSqlConnection)
                                {
                                    DestinationTableName = "[##FAMILYEXPTEMPFAM" + ssid + "]"
                                };
                                bulkCopy_cp.WriteToServer(familyExport);
                                _sqlString = "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''[##FAMILYEXPTEMP" + ssid + "]'') BEGIN DROP TABLE [##FAMILYEXPTEMP" + ssid + "] END')";
                                SqlCommand _DBCommand1 = new SqlCommand(_sqlString, objSqlConnection);
                                _DBCommand1.ExecuteNonQuery();

                                _sqlString = CreateTable("[##FAMILYEXPTEMP" + ssid + "]", objDataSet.Tables[0]);
                                SqlCommand _DBCommandn1 = new SqlCommand(_sqlString, objSqlConnection);
                                _DBCommandn1.ExecuteNonQuery();

                                var bulkCopy_cp1 = new SqlBulkCopy(objSqlConnection)
                                {
                                    DestinationTableName = "[##FAMILYEXPTEMP" + ssid + "]"
                                };
                                bulkCopy_cp1.WriteToServer(objDataSet.Tables[0]);
                                objDataSet = new DataSet();

                                _sqlString = "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''[##FAMILYEXP" + ssid + "]'') BEGIN DROP TABLE [##FAMILYEXP" + ssid + "] END')";
                                SqlCommand _DBCommandc = new SqlCommand(_sqlString, objSqlConnection);
                                _DBCommandc.ExecuteNonQuery();

                                _sqlString = "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''[##FAMILYEXPFAM" + ssid + "]'') BEGIN DROP TABLE [##FAMILYEXPFAM" + ssid + "] END')";
                                _DBCommandc = new SqlCommand(_sqlString, objSqlConnection);
                                _DBCommandc.ExecuteNonQuery();

                                if (!string.IsNullOrEmpty(fromdate) && !string.IsNullOrEmpty(todate))
                                {
                                    _sqlString = "declare @frm varchar(100)" +
                                       "declare @to varchar(100)" +
                                       "set @frm = (select cast(CONVERT(varchar, '" + fromdate + "', 101) as date))" +
                                       "set @to = (select cast(CONVERT(varchar, '" + todate + "', 101) as date))" +
                                       "exec('SELECT distinct  PRODUCT_ID into [##Temp" + ssid + "] FROM TB_PROD_SPECS WHERE cast(CONVERT(varchar, MODIFIED_DATE, 101) as date) BETWEEN '''+@frm+'''  AND '''+@to+'''')" +
                                       "EXEC('select * into [##FAMILYEXP" + ssid + "] from  [##FAMILYEXPTEMP" + ssid + "] where product_id in (select distinct product_id from [##Temp" + ssid + "])')";
                                    SqlCommand _DBCommandcc = new SqlCommand(_sqlString, objSqlConnection);
                                    _DBCommandcc.ExecuteNonQuery();

                                    _sqlString = "declare @frm varchar(100)" +
                                       "declare @to varchar(100)" +
                                       "set @frm = (select cast(CONVERT(varchar, '" + fromdate + "', 101) as date))" +
                                       "set @to = (select cast(CONVERT(varchar, '" + todate + "', 101) as date))" +
                                       "exec('SELECT distinct  FAMILY_ID into [##TempFamly" + ssid + "] FROM TB_FAMILY_SPECS WHERE cast(CONVERT(varchar, MODIFIED_DATE, 101) as date) BETWEEN '''+@frm+'''  AND '''+@to+'''')" +
                                       "EXEC('select * into [##FAMILYEXPFAM" + ssid + "] from  [##FAMILYEXPTEMPFAM" + ssid + "] where FAMILY_ID in (select FAMILY_ID from [##TempFamly" + ssid + "])')";
                                    _DBCommandcc = new SqlCommand(_sqlString, objSqlConnection);
                                    _DBCommandcc.ExecuteNonQuery();
                                }
                                else
                                {
                                    _sqlString =
                                      "EXEC('select * into [##FAMILYEXP" + ssid + "] from  [##FAMILYEXPTEMP" + ssid + "]')";
                                    SqlCommand _DBCommandcc = new SqlCommand(_sqlString, objSqlConnection);
                                    _DBCommandcc.ExecuteNonQuery();

                                    _sqlString =
                                    "EXEC('select * into [##FAMILYEXPFAM" + ssid + "] from  [##FAMILYEXPTEMPFAM" + ssid + "]')";
                                    _DBCommandcc = new SqlCommand(_sqlString, objSqlConnection);
                                    _DBCommandcc.ExecuteNonQuery();
                                }
                                _logger.Info("FOR SUB PRODUCTS FAMILY SELECTION");
                                //----------------------------------FOR SUB PRODUCTS FAMILY SELECTION ----------------------------
                                //if (exporsubproducts == "Yes")
                                if (EnableSubProduct == true)
                                {
                                    string _sqlStringSP = null;
                                    _sqlStringSP = "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''[##SUBPRO" +
                                                ssidsp + "]'') BEGIN DROP TABLE [##SUBPRO" + ssidsp + "] END')";
                                    SqlCommand _DBCommandsp = new SqlCommand(_sqlStringSP, objSqlConnection);
                                    _DBCommandsp.ExecuteNonQuery();

                                    _sqlStringSP = CreateTable("[##SUBPRO" + ssidsp + "]", dt_SP);
                                    SqlCommand _DBCommandnsp = new SqlCommand(_sqlStringSP, objSqlConnection);
                                    _DBCommandnsp.ExecuteNonQuery();

                                    var bulkCopy_cpsp = new SqlBulkCopy(objSqlConnection)
                                    {
                                        DestinationTableName = "[##SUBPRO" + ssidsp + "]"
                                    };
                                    bulkCopy_cpsp.WriteToServer(dt_SP);
                                }
                                objDataSet = new DataSet();
                                objDataSetFam = new DataTable();
                                if (string.IsNullOrEmpty(selfamily_id))
                                {
                                    var objSqlDataAdapterexp = new SqlDataAdapter(
                                        "select * from [##FAMILYEXP" + ssid + "]", objSqlConnection);
                                    objSqlDataAdapterexp.Fill(objDataSet);

                                    string _sqlStringFilter = null;
                                    _sqlStringFilter =
                                                "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''[##FAMILYEXPFILTER" +
                                                ssid + "]'') BEGIN DROP TABLE [##FAMILYEXPFILTER" + ssid + "] END')";
                                    SqlCommand _DBCommandFam = new SqlCommand(_sqlStringFilter, objSqlConnection);
                                    _DBCommandFam.ExecuteNonQuery();

                                    _sqlStringFilter = CreateTable("[##FAMILYEXPFILTER" + ssid + "]", objDataSet.Tables[0]);
                                    SqlCommand _DBCommandnFam = new SqlCommand(_sqlStringFilter, objSqlConnection);
                                    _DBCommandnFam.ExecuteNonQuery();

                                    var bulkCopy_cpSelectFamily = new SqlBulkCopy(objSqlConnection)
                                    {
                                        DestinationTableName = "[##FAMILYEXPFILTER" + ssid + "]"
                                    };
                                    bulkCopy_cpSelectFamily.WriteToServer(objDataSet.Tables[0]);

                                    objSqlDataAdapterexp = new SqlDataAdapter("select * from [##FAMILYEXPFAM" + ssid + "]", objSqlConnection);
                                    objSqlDataAdapterexp.Fill(objDataSetFam);

                                    _sqlStringFilter = null;
                                    _sqlStringFilter = "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''[##FAMILYEXPFILTERFAM" + ssid + "]'') BEGIN DROP TABLE [##FAMILYEXPFILTERFAM" + ssid + "] END')";
                                    _DBCommandFam = new SqlCommand(_sqlStringFilter, objSqlConnection);
                                    _DBCommandFam.ExecuteNonQuery();

                                    _sqlStringFilter = CreateTable("[##FAMILYEXPFILTERFAM" + ssid + "]", familyExport);
                                    _DBCommandnFam = new SqlCommand(_sqlStringFilter, objSqlConnection);
                                    _DBCommandnFam.ExecuteNonQuery();

                                    bulkCopy_cpSelectFamily = new SqlBulkCopy(objSqlConnection)
                                    {
                                        DestinationTableName = "[##FAMILYEXPFILTERFAM" + ssid + "]"
                                    };
                                    bulkCopy_cpSelectFamily.WriteToServer(familyExport);
                                }
                                _logger.Info("Family level Filtering based on selected Families");
                                //--------------------------Family level Filtering based on selected Families ----------------------//
                                if (!string.IsNullOrEmpty(selfamily_id))
                                {
                                    objDataSet = new DataSet();
                                    if (selfamily_id == "All" || selfamily_id == "")
                                    {
                                        var objSqlDataAdapterexp = new SqlDataAdapter("select * from [##FAMILYEXP" + ssid + "]", objSqlConnection);
                                        objSqlDataAdapterexp.Fill(objDataSet);
                                    }
                                    else
                                    {
                                        var objSqlDataAdapterexp1 = new SqlDataAdapter("select * from [##FAMILYEXP" + ssid + "] where family_id in (" + selfamily_id + ") ", objSqlConnection);
                                        objSqlDataAdapterexp1.Fill(objDataSet);
                                        //--------------------------------------------------------------------
                                        string _sqlStringFilter = null;
                                        _sqlStringFilter = "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''[##FAMILYEXPFILTER" + ssid + "]'') BEGIN DROP TABLE [##FAMILYEXPFILTER" + ssid + "] END')";
                                        SqlCommand _DBCommandFam = new SqlCommand(_sqlStringFilter, objSqlConnection);
                                        _DBCommandFam.ExecuteNonQuery();

                                        _sqlStringFilter = CreateTable("[##FAMILYEXPFILTER" + ssid + "]", objDataSet.Tables[0]);
                                        SqlCommand _DBCommandnFam = new SqlCommand(_sqlStringFilter, objSqlConnection);
                                        _DBCommandnFam.ExecuteNonQuery();

                                        var bulkCopy_cpSelectFamily = new SqlBulkCopy(objSqlConnection)
                                        {
                                            DestinationTableName = "[##FAMILYEXPFILTER" + ssid + "]"
                                        };
                                        bulkCopy_cpSelectFamily.WriteToServer(objDataSet.Tables[0]);
                                    }

                                    if (selfamily_id == "All" || selfamily_id == "")
                                    {
                                        var objSqlDataAdapterexp = new SqlDataAdapter("select * from [##FAMILYEXPFAM" + ssid + "]", objSqlConnection);
                                        objSqlDataAdapterexp.Fill(objDataSetFam);
                                        familyExport.Clear();
                                        familyExport.Columns.Clear();
                                        familyExport = objDataSetFam;
                                    }
                                    else
                                    {
                                        var objSqlDataAdapterexp1 = new SqlDataAdapter("select * from [##FAMILYEXPFAM" + ssid + "] where family_id in (" + selfamily_id + ") ", objSqlConnection);
                                        objSqlDataAdapterexp1.Fill(objDataSetFam);
                                        familyExport.Clear();
                                        familyExport.Columns.Clear();
                                        familyExport = objDataSetFam;
                                        //--------------------------------------------------------------------
                                        string _sqlStringFilter = null;
                                        _sqlStringFilter = "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''[##FAMILYEXPFILTERFAM" + ssid + "]'') BEGIN DROP TABLE [##FAMILYEXPFILTERFAM" + ssid + "] END')";
                                        SqlCommand _DBCommandFam = new SqlCommand(_sqlStringFilter, objSqlConnection);
                                        _DBCommandFam.ExecuteNonQuery();

                                        _sqlStringFilter = CreateTable("[##FAMILYEXPFILTERFAM" + ssid + "]", familyExport);
                                        SqlCommand _DBCommandnFam = new SqlCommand(_sqlStringFilter, objSqlConnection);
                                        _DBCommandnFam.ExecuteNonQuery();

                                        var bulkCopy_cpSelectFamily = new SqlBulkCopy(objSqlConnection)
                                        {
                                            DestinationTableName = "[##FAMILYEXPFILTERFAM" + ssid + "]"
                                        };
                                        bulkCopy_cpSelectFamily.WriteToServer(familyExport);
                                    }
                                }
                                if (selectedAttributes.Count > 0)
                                {
                                    int cntVal = 1;
                                    foreach (var item in selectedAttributes)
                                    {
                                        var tempattrdataset = new DataSet();
                                        string attrName = item["ATTRIBUTE_NAME"].ToString();
                                        string attrValue = item["STRING_VALUE"].ToString();
                                        if (objDataSet.Tables[0].Columns.Contains(attrName))
                                        {
                                            SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                                            objSqlCommand.CommandText = "STP_LS_GET_EXPORT_ATTRIBUTE_VALUES";
                                            objSqlCommand.CommandType = CommandType.StoredProcedure;
                                            objSqlCommand.CommandTimeout = 0;
                                            objSqlCommand.Connection = objSqlConnection;
                                            objSqlCommand.Parameters.Add("@SESSID", SqlDbType.NVarChar, 100).Value = ssid;
                                            objSqlCommand.Parameters.Add("@SEARCHEXP", SqlDbType.NVarChar).Value = attrName;
                                            objSqlCommand.Parameters.Add("@SEARCHVALUE", SqlDbType.NVarChar).Value = attrValue;
                                            objSqlCommand.Parameters.Add("@CNTVALUE", SqlDbType.Int).Value = cntVal;
                                            var objSqlDataAdapter = new SqlDataAdapter(objSqlCommand);
                                            objSqlDataAdapter.Fill(tempattrdataset);

                                            _logger.Info("FOR FILTERING DUPLICATE VALUES");

                                            //---------------FOR FILTERING DUPLICATE VALUES ---------------------------//
                                            if (tempattrdataset.Tables.Count > 0 && tempattrdataset.Tables[0].Rows.Count > 0)
                                            {
                                                var distinctds = tempattrdataset.Tables[0].DefaultView.ToTable(true);
                                            }
                                            attributedataset = tempattrdataset;
                                        }
                                        cntVal = cntVal + 1;
                                    }
                                    objDataSet = attributedataset;

                                    _logger.Info("FOR SUP PRODUCTS FILTERATION");

                                    //---------------------------FOR SUP PRODUCTS FILTERATION -----------------------------
                                    //if (exporsubproducts == "Yes")
                                    if (EnableSubProduct == true)
                                    {
                                        string _sqlStringSPF = null;
                                        _sqlStringSPF = "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''[##SUBPROF" + ssidspf + "]'') BEGIN DROP TABLE [##SUBPROF" + ssidspf + "] END')";
                                        SqlCommand _DBCommandspf = new SqlCommand(_sqlStringSPF, objSqlConnection);
                                        _DBCommandspf.ExecuteNonQuery();

                                        _sqlStringSPF = CreateTable("[##SUBPROF" + ssidspf + "]", objDataSet.Tables[0]);
                                        SqlCommand _DBCommandnspf = new SqlCommand(_sqlStringSPF, objSqlConnection);
                                        _DBCommandnspf.ExecuteNonQuery();

                                        var bulkCopy_cpspf = new SqlBulkCopy(objSqlConnection)
                                        {
                                            DestinationTableName = "[##SUBPROF" + ssidspf + "]"
                                        };
                                        bulkCopy_cpspf.WriteToServer(objDataSet.Tables[0]);
                                        //--------------------------------------------------------------------------------------
                                        if (!string.IsNullOrEmpty(selfamily_id))
                                        {
                                            if (selfamily_id == "All" || selfamily_id == "")
                                            {
                                                objDataSet.Tables.Add(dt_SP);
                                            }
                                            else
                                            {
                                                var objSqlDataAdapterexp1 = new SqlDataAdapter("select * from [##SUBPRO" + ssidsp + "] where family_id in (" + selfamily_id + ")  and product_id in (select distinct product_id  from [##SUBPROF" + ssidspf + "])", objSqlConnection);
                                                objSqlDataAdapterexp1.Fill(dt_SP_sel);
                                                objDataSet.Tables.Add(dt_SP_sel);
                                            }
                                        }
                                    }
                                    _logger.Info("FOR RETURNIG EMPTY RECORDS IF NOTHING FOUND");
                                    //---------------------FOR RETURNIG EMPTY RECORDS IF NOTHING FOUND--------------------//
                                    if (objDataSet.Tables[0].Rows.Count == 0)
                                        objDataSet.Tables[0].Rows.Add();
                                }
                                _logger.Info("FOR NO ATTRIBUTE SELECTIION");
                                //-------------------------FOR NO ATTRIBUTE SELECTIION----------------------------------//
                                if (selectedAttributes.Count == 0)
                                {
                                    //if (exporsubproducts == "Yes")
                                    if (EnableSubProduct == true)
                                    {
                                        DataTable subproductdt = new DataTable();
                                        if (!string.IsNullOrEmpty(selfamily_id))
                                        {
                                            var objSqlDataAdapterexp1 = new SqlDataAdapter("select * from [##SUBPRO" + ssidsp + "] where family_id in (" + selfamily_id + ")", objSqlConnection);
                                            objSqlDataAdapterexp1.Fill(subproductdt);
                                            objDataSet.Tables.Add(subproductdt);
                                            //objDataSet.Tables.Add(dt_SP);
                                        }
                                        else
                                        {
                                            var objSqlDataAdapterexp1 = new SqlDataAdapter("select * from [##SUBPRO" + ssidsp + "]", objSqlConnection);
                                            objSqlDataAdapterexp1.Fill(subproductdt);
                                            objDataSet.Tables.Add(subproductdt);
                                        }
                                    }
                                }
                            }
                            else if (selcategory_id == "All" && attrBy == "Row")
                            {
                                //var catalogForm = new TradingBell.CatalogX.CatalogXfunction();
                                //objDataSet = catalogForm.CatalogXRow(catalogid, 0, allintcustomAttr,
                                //    ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString, "");
                                objDataSet = objcustomExport.CatalogXRow(catalogid, 0, allintcustomAttr, "");

                            }
                            else if (attrBy == "Row")
                            {
                                string ssid = Guid.NewGuid().ToString();
                                var rowtable = new DataSet();
                                objDataSet = new DataSet();
                                foreach (var item in Row_categories)
                                {
                                    categoryname = item.ToString();
                                    categoryname = categoryname.ToUpper();
                                    //var catalogForm = new TradingBell.CatalogX.CatalogXfunction();
                                    //objDataSet = catalogForm.CatalogCategoryXRow(catalogid, 0, allintcustomAttr,
                                    //    ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString, categoryname);
                                    //objDataSet = objcustomExport.CatalogCategoryXRow(catalogid, 0, allintcustomAttr, categoryname);
                                    tempdataset = objcustomExport.CatalogCategoryXRow(catalogid, 0, allintcustomAttr, categoryname);
                                    //------------------------
                                    using (var conn = new SqlConnection(sqlConn))
                                    {
                                        conn.Open();
                                        // string _sqlString = null;
                                        _sqlString = "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''[##FAMILYEXPROW" + ssid + "]'')BEGIN DROP TABLE [##FAMILYEXPROW" + ssid + "] END')";
                                        SqlCommand _DBCommand = new SqlCommand(_sqlString, conn);
                                        _DBCommand.ExecuteNonQuery();

                                        _sqlString = CreateTable("[##FAMILYEXPROW" + ssid + "]", tempdataset.Tables[3]);
                                        SqlCommand _DBCommandn = new SqlCommand(_sqlString, conn);
                                        _DBCommandn.ExecuteNonQuery();

                                        var bulkCopy_cp = new SqlBulkCopy(conn)
                                        {
                                            DestinationTableName = "[##FAMILYEXPROW" + ssid + "]"
                                        };
                                        bulkCopy_cp.WriteToServer(tempdataset.Tables[3]);

                                        var objSqlDataAdapterexp = new SqlDataAdapter("select * from [##FAMILYEXPROW" + ssid + "]", conn);
                                        objSqlDataAdapterexp.Fill(rowtable);
                                        objDataSet.Merge(rowtable);
                                    }
                                }
                                _logger.Info("Family level Filtering based on selected Families");
                                //--------------------------Created by VinothKumar For Family level Filtering based on selected Families ----------------------//
                                if (!string.IsNullOrEmpty(selfamily_id))
                                {
                                    using (var conn = new SqlConnection(sqlConn))
                                    {
                                        conn.Open();
                                        // string _sqlString = null;
                                        _sqlString =
                                                    "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''[##FAMILYEXP" +
                                                    ssid + "]'')BEGIN DROP TABLE [##FAMILYEXP" + ssid + "] END')";
                                        SqlCommand _DBCommand = new SqlCommand(_sqlString, conn);
                                        _DBCommand.ExecuteNonQuery();

                                        _sqlString = CreateTable("[##FAMILYEXP" + ssid + "]", objDataSet.Tables[0]);
                                        SqlCommand _DBCommandn = new SqlCommand(_sqlString, conn);
                                        _DBCommandn.ExecuteNonQuery();

                                        var bulkCopy_cp = new SqlBulkCopy(conn)
                                        {
                                            DestinationTableName = "[##FAMILYEXP" + ssid + "]"
                                        };
                                        bulkCopy_cp.WriteToServer(objDataSet.Tables[0]);

                                        objDataSet = new DataSet();
                                        var objSqlDataAdapterexp = new SqlDataAdapter("select * from [##FAMILYEXP" + ssid + "] where family_id in (" + selfamily_id + ") ", conn);
                                        objSqlDataAdapterexp.Fill(objDataSet);
                                    }
                                }
                            }
                        } // sql conn end
                        if (objDataSet.Tables[0].Columns.Contains("FAMILY_ID_1"))
                        {
                            objDataSet.Tables[0].Columns.Remove("FAMILY_ID_1");
                        }
                        if (familyFilter == "Yes")
                        {
                            if (objDataSet.Tables.Count > 0)
                            {
                                foreach (DataRow dr in objDataSet.Tables[0].Rows)
                                {
                                    if (dr["FAMILY_ID"].ToString() != familyId.ToString())
                                        dr.Delete();
                                }
                                objDataSet.AcceptChanges();
                            }
                        }
                        if (objDataSet.Tables[0].Rows.Count > 0)
                        {
                            foreach (DataRow dr in objDataSet.Tables[0].Rows)
                            {
                                var temp = new DataSet();
                                //if ((dr["A"].ToString() != null && dr["B"].ToString() != null && dr["C"].ToString() != null) || (dr["A"].ToString() != "" && dr["B"].ToString() != "" && dr["C"].ToString() != ""))
                                //{
                                if (!string.IsNullOrEmpty(Convert.ToString(dr["FAMILY_ID"])))
                                {
                                    if (objDataSet.Tables[0].Columns.Contains("PRODUCT_ID"))
                                    {
                                        if (!string.IsNullOrEmpty(Convert.ToString(dr["PRODUCT_ID"])))
                                        {
                                            if (!string.IsNullOrEmpty(Convert.ToString(dr["SUBFAMILY_ID"])))
                                            {
                                                using (var objSqlConnection = new SqlConnection(sqlConn))
                                                {
                                                    SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                                                    objSqlCommand.CommandText = "STP_QS_LS_PICKEXPORTATTRIBUTE";
                                                    objSqlCommand.CommandType = CommandType.StoredProcedure;
                                                    objSqlCommand.CommandTimeout = 0;
                                                    objSqlCommand.Connection = objSqlConnection;
                                                    objSqlCommand.Parameters.Add("@OPTION", SqlDbType.NVarChar, 100).Value = "PRODSUBFAMILY";
                                                    objSqlCommand.Parameters.Add("@PRODUCT_ID", SqlDbType.Int).Value = Convert.ToInt32(dr["PRODUCT_ID"]);
                                                    objSqlCommand.Parameters.Add("@SUBFAMILY_ID", SqlDbType.Int).Value = Convert.ToInt32(dr["SUBFAMILY_ID"]);
                                                    objSqlConnection.Open();
                                                    var objSqlDataAdapter = new SqlDataAdapter(objSqlCommand);
                                                    objSqlDataAdapter.Fill(temp);
                                                }
                                            }
                                            else
                                            {
                                                using (var objSqlConnection = new SqlConnection(sqlConn))
                                                {
                                                    SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                                                    objSqlCommand.CommandText = "STP_QS_LS_PICKEXPORTATTRIBUTE";
                                                    objSqlCommand.CommandType = CommandType.StoredProcedure;
                                                    objSqlCommand.CommandTimeout = 0;
                                                    objSqlCommand.Connection = objSqlConnection;
                                                    objSqlCommand.Parameters.Add("@OPTION", SqlDbType.NVarChar, 100).Value = "PRODSUBFAMILY";
                                                    objSqlCommand.Parameters.Add("@PRODUCT_ID", SqlDbType.Int).Value = Convert.ToInt32(dr["PRODUCT_ID"]);
                                                    objSqlCommand.Parameters.Add("@SUBFAMILY_ID", SqlDbType.Int).Value = Convert.ToInt32(dr["FAMILY_ID"]);
                                                    objSqlConnection.Open();
                                                    var objSqlDataAdapter = new SqlDataAdapter(objSqlCommand);
                                                    objSqlDataAdapter.Fill(temp);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            using (var objSqlConnection = new SqlConnection(sqlConn))
                                            {
                                                SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                                                objSqlCommand.CommandText = "STP_QS_LS_PICKEXPORTATTRIBUTE";
                                                objSqlCommand.CommandType = CommandType.StoredProcedure;
                                                objSqlCommand.CommandTimeout = 0;
                                                objSqlCommand.Connection = objSqlConnection;
                                                objSqlCommand.Parameters.Add("@OPTION", SqlDbType.NVarChar, 100).Value = "FAMILY";
                                                objSqlCommand.Parameters.Add("@SUBFAMILY_ID", SqlDbType.Int).Value = Convert.ToInt32(dr["FAMILY_ID"]);
                                                objSqlConnection.Open();
                                                var objSqlDataAdapter = new SqlDataAdapter(objSqlCommand);
                                                objSqlDataAdapter.Fill(temp);
                                            }
                                            // dbConn.SQLString = "select ATTRIBUTE_NAME,ATTRIBUTE_DATATYPE  from TB_FAMILY_SPECS aps join TB_ATTRIBUTE b on aps.ATTRIBUTE_ID=b.ATTRIBUTE_ID where FAMILY_ID=" + dr["FAMILY_ID"].ToString() + " and (ATTRIBUTE_DATATYPE like 'Num%' or ATTRIBUTE_DATATYPE like 'Date and Time%')";
                                        }
                                    }
                                    else
                                    {
                                        using (var objSqlConnection = new SqlConnection(sqlConn))
                                        {
                                            SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                                            objSqlCommand.CommandText = "STP_QS_LS_PICKEXPORTATTRIBUTE";
                                            objSqlCommand.CommandType = CommandType.StoredProcedure;
                                            objSqlCommand.Connection = objSqlConnection;
                                            objSqlCommand.CommandTimeout = 0;
                                            objSqlCommand.Parameters.Add("@OPTION", SqlDbType.NVarChar, 100).Value = "FAMILY";
                                            objSqlCommand.Parameters.Add("@SUBFAMILY_ID", SqlDbType.Int).Value = Convert.ToInt32(dr["FAMILY_ID"]);
                                            objSqlConnection.Open();
                                            var objSqlDataAdapter = new SqlDataAdapter(objSqlCommand);
                                            objSqlDataAdapter.Fill(temp);
                                        }
                                    }
                                    if (temp.Tables.Count > 0 && temp.Tables[0].Rows.Count > 0)
                                    {
                                        for (int i = 0; i < objDataSet.Tables[0].Columns.Count; i++)
                                        {
                                            for (int j = 0; j < temp.Tables[0].Rows.Count; j++)
                                            {
                                                if (Convert.ToString(temp.Tables[0].Rows[j].ItemArray[0]) == Convert.ToString(objDataSet.Tables[0].Columns[i]))
                                                {
                                                    if (!String.IsNullOrEmpty(Convert.ToString(dr[objDataSet.Tables[0].Columns[i]])))
                                                    {
                                                        if (Convert.ToString(temp.Tables[0].Rows[j].ItemArray[1]).StartsWith("Num"))
                                                        {
                                                            string strval = Convert.ToString(dr[Convert.ToString(objDataSet.Tables[0].Columns[i])]);
                                                            string datatype = Convert.ToString(temp.Tables[0].Rows[j].ItemArray[1]);
                                                            datatype = datatype.Remove(0, datatype.IndexOf(',') + 1);
                                                            int noofdecimalplace = Convert.ToInt32(datatype.TrimEnd(')'));
                                                            if (noofdecimalplace != 6)
                                                            {
                                                                if (strval.Contains("."))
                                                                {
                                                                    if ((strval.IndexOf('.') + 1 + noofdecimalplace) != (strval.Length))
                                                                    {
                                                                        if (strval.Length >= strval.IndexOf('.') + 1 + noofdecimalplace)
                                                                            strval = strval.Remove(strval.IndexOf('.') + 1 + noofdecimalplace);
                                                                    }
                                                                }
                                                            }
                                                            if (noofdecimalplace == 0)
                                                            {
                                                                if (strval.Contains("."))
                                                                    strval = strval.TrimEnd('.');
                                                            }
                                                            dr[objDataSet.Tables[0].Columns[i]] = strval;
                                                        }
                                                        if (Convert.ToString(temp.Tables[0].Rows[j].ItemArray[1]).StartsWith("Date and Time"))
                                                        {
                                                            string attribute_name = Convert.ToString(temp.Tables[0].Rows[j].ItemArray[0]);

                                                            DataTable attr = new DataTable();
                                                            String query = "select * from TB_ATTRIBUTE where ATTRIBUTE_NAME = '" + attribute_name + "'";
                                                            SqlCommand cmd2 = new SqlCommand(query, sqlConnection);
                                                            sqlConnection.Open();
                                                            sqlDataAdapter = new SqlDataAdapter(cmd2);

                                                            sqlDataAdapter.Fill(attr);
                                                            var selattributeDataFormat = attr.AsEnumerable().ToList();
                                                            sqlConnection.Close();

                                                            //================= var selattributeDataFormat = _dbcontext.TB_ATTRIBUTE.FirstOrDefault(y => y.ATTRIBUTE_NAME == attribute_name);


                                                            String getAttributeDataFormat = string.Empty;
                                                            if (selattributeDataFormat != null)

                                                                getAttributeDataFormat = attr.Rows[0]["ATTRIBUTE_NAME"].ToString();

                                                            if (!string.IsNullOrEmpty(getAttributeDataFormat))
                                                            {
                                                                if (getAttributeDataFormat.StartsWith("(?=\\d\\d(\\-|\\/|\\.)\\d\\d(\\-|\\/|\\.)\\d{4})(?=.{0}(?:0[1-9]|[12]\\d|3[01]))(?=.{3}"))
                                                                {
                                                                    string tempdate = Convert.ToString(dr[objDataSet.Tables[0].Columns[i]]);
                                                                    dr[objDataSet.Tables[0].Columns[i]] = tempdate.Substring(3, 2) + "/" + tempdate.Substring(0, 2) + "/" + tempdate.Substring(6, 4);
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                //}
                            }
                        }

                        _logger.Info("SUB Product Export START");

                        /***************** SUB Product Export START *********************/
                        if (familyFilter == "Yes")
                        {
                            if (objDataSet.Tables.Count > 0)
                            {
                                foreach (DataRow dr in objDataSet.Tables[1].Rows)
                                {
                                    if (dr["FAMILY_ID"].ToString() != familyId.ToString())
                                        dr.Delete();
                                }
                                objDataSet.AcceptChanges();
                            }
                        }
                        if (familyFilter == "Yes")
                        {
                            if (objDataSet.Tables.Count > 2)
                            {
                                foreach (DataRow dr in objDataSet.Tables[3].Rows)
                                {
                                    if (dr["FAMILY_ID"].ToString() != familyId.ToString())
                                        dr.Delete();
                                }
                                objDataSet.AcceptChanges();
                            }
                            if (objDataSet.Tables.Count > 3)
                            {
                                foreach (DataRow dr in objDataSet.Tables[4].Rows)
                                {
                                    if (dr["FAMILY_ID"].ToString() != familyId.ToString())
                                        dr.Delete();
                                }
                                objDataSet.AcceptChanges();
                            }
                        }
                        if (objDataSet.Tables.Count > 1)
                        {
                            if (objDataSet.Tables[1].Rows.Count > 0 && attrBy != "Row")
                            {
                                DataColumnCollection columns = objDataSet.Tables[1].Columns;

                                if (columns.Contains("Item #"))
                                    objDataSet.Tables[1].Columns.Remove("Item #");
                                if (columns.Contains("Item#"))
                                    objDataSet.Tables[1].Columns.Remove("Item#");

                                foreach (DataRow dr in objDataSet.Tables[1].Rows)
                                {
                                    var temp = new DataSet();
                                    //if ((dr["A"].ToString() != null && dr["B"].ToString() != null && dr["C"].ToString() != null) || (dr["A"].ToString() != "" && dr["B"].ToString() != "" && dr["C"].ToString() != ""))
                                    // {
                                    if (!string.IsNullOrEmpty(Convert.ToString(dr["FAMILY_ID"])))
                                    {
                                        if (objDataSet.Tables[1].Columns.Contains("PRODUCT_ID"))
                                        {
                                            if (!string.IsNullOrEmpty(Convert.ToString(dr["PRODUCT_ID"])))
                                            {

                                                if (!string.IsNullOrEmpty(Convert.ToString(dr["SUBFAMILY_ID"])))
                                                {
                                                    using (var objSqlConnection = new SqlConnection(sqlConn))
                                                    {
                                                        SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                                                        objSqlCommand.CommandText = "STP_QS_LS_PICKEXPORTATTRIBUTE";
                                                        objSqlCommand.CommandType = CommandType.StoredProcedure;
                                                        objSqlCommand.CommandTimeout = 0;
                                                        objSqlCommand.Connection = objSqlConnection;
                                                        objSqlCommand.Parameters.Add("@OPTION", SqlDbType.NVarChar, 100).Value = "PRODSUBFAMILY";
                                                        objSqlCommand.Parameters.Add("@PRODUCT_ID", SqlDbType.Int).Value = Convert.ToInt32(dr["PRODUCT_ID"]);
                                                        objSqlCommand.Parameters.Add("@SUBFAMILY_ID", SqlDbType.Int).Value = Convert.ToInt32(dr["SUBFAMILY_ID"]);
                                                        objSqlConnection.Open();
                                                        var objSqlDataAdapter = new SqlDataAdapter(objSqlCommand);
                                                        objSqlDataAdapter.Fill(temp);
                                                    }
                                                }
                                                else
                                                {
                                                    using (var objSqlConnection = new SqlConnection(sqlConn))
                                                    {
                                                        SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                                                        objSqlCommand.CommandText = "STP_QS_LS_PICKEXPORTATTRIBUTE";
                                                        objSqlCommand.CommandType = CommandType.StoredProcedure;
                                                        objSqlCommand.CommandTimeout = 0;
                                                        objSqlCommand.Connection = objSqlConnection;
                                                        objSqlCommand.Parameters.Add("@OPTION", SqlDbType.NVarChar, 100).Value = "PRODSUBFAMILY";
                                                        objSqlCommand.Parameters.Add("@PRODUCT_ID", SqlDbType.Int).Value = Convert.ToInt32(dr["PRODUCT_ID"]);
                                                        objSqlCommand.Parameters.Add("@SUBFAMILY_ID", SqlDbType.Int).Value = Convert.ToInt32(dr["FAMILY_ID"]);
                                                        objSqlConnection.Open();
                                                        var objSqlDataAdapter = new SqlDataAdapter(objSqlCommand);
                                                        objSqlDataAdapter.Fill(temp);
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                using (var objSqlConnection = new SqlConnection(sqlConn))
                                                {
                                                    SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                                                    objSqlCommand.CommandText = "STP_QS_LS_PICKEXPORTATTRIBUTE";
                                                    objSqlCommand.CommandType = CommandType.StoredProcedure;
                                                    objSqlCommand.CommandTimeout = 0;
                                                    objSqlCommand.Connection = objSqlConnection;
                                                    objSqlCommand.Parameters.Add("@OPTION", SqlDbType.NVarChar, 100).Value = "FAMILY";
                                                    objSqlCommand.Parameters.Add("@SUBFAMILY_ID", SqlDbType.Int).Value = Convert.ToInt32(dr["FAMILY_ID"]);
                                                    objSqlConnection.Open();
                                                    var objSqlDataAdapter = new SqlDataAdapter(objSqlCommand);
                                                    objSqlDataAdapter.Fill(temp);
                                                }
                                                // dbConn.SQLString = "select ATTRIBUTE_NAME,ATTRIBUTE_DATATYPE  from TB_FAMILY_SPECS aps join TB_ATTRIBUTE b on aps.ATTRIBUTE_ID=b.ATTRIBUTE_ID where FAMILY_ID=" + dr["FAMILY_ID"].ToString() + " and (ATTRIBUTE_DATATYPE like 'Num%' or ATTRIBUTE_DATATYPE like 'Date and Time%')";
                                            }
                                        }
                                        else
                                        {
                                            using (var objSqlConnection = new SqlConnection(sqlConn))
                                            {
                                                SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                                                objSqlCommand.CommandText = "STP_QS_LS_PICKEXPORTATTRIBUTE";
                                                objSqlCommand.CommandType = CommandType.StoredProcedure;
                                                objSqlCommand.Connection = objSqlConnection;
                                                objSqlCommand.CommandTimeout = 0;
                                                objSqlCommand.Parameters.Add("@OPTION", SqlDbType.NVarChar, 100).Value = "FAMILY";
                                                objSqlCommand.Parameters.Add("@SUBFAMILY_ID", SqlDbType.Int).Value = Convert.ToInt32(dr["FAMILY_ID"]);
                                                objSqlConnection.Open();
                                                var objSqlDataAdapter = new SqlDataAdapter(objSqlCommand);
                                                objSqlDataAdapter.Fill(temp);
                                            }
                                        }
                                        if (temp.Tables.Count > 1 && temp.Tables[0].Rows.Count > 0)
                                        {
                                            for (int i = 0; i < objDataSet.Tables[1].Columns.Count; i++)
                                            {
                                                for (int j = 0; j < temp.Tables[0].Rows.Count; j++)
                                                {
                                                    if (Convert.ToString(temp.Tables[1].Rows[j].ItemArray[0]) == Convert.ToString(objDataSet.Tables[1].Columns[i]))
                                                    {
                                                        if (!String.IsNullOrEmpty(Convert.ToString(dr[objDataSet.Tables[1].Columns[i]])))
                                                        {
                                                            if (Convert.ToString(temp.Tables[1].Rows[j].ItemArray[1]).StartsWith("Num"))
                                                            {
                                                                string strval = Convert.ToString(dr[Convert.ToString(objDataSet.Tables[1].Columns[i])]);
                                                                string datatype = Convert.ToString(temp.Tables[0].Rows[j].ItemArray[1]);
                                                                datatype = datatype.Remove(0, datatype.IndexOf(',') + 1);
                                                                int noofdecimalplace = Convert.ToInt32(datatype.TrimEnd(')'));
                                                                if (noofdecimalplace != 6)
                                                                {
                                                                    if (strval.Contains("."))
                                                                    {
                                                                        if ((strval.IndexOf('.') + 1 + noofdecimalplace) != (strval.Length))
                                                                        {
                                                                            if (strval.Length >= strval.IndexOf('.') + 1 + noofdecimalplace)
                                                                                strval = strval.Remove(strval.IndexOf('.') + 1 + noofdecimalplace);
                                                                        }
                                                                    }
                                                                }
                                                                if (noofdecimalplace == 0)
                                                                {
                                                                    strval = strval.TrimEnd('.');
                                                                }
                                                                dr[objDataSet.Tables[1].Columns[i]] = strval;
                                                            }
                                                            if (Convert.ToString(temp.Tables[0].Rows[j].ItemArray[1]).StartsWith("Date and Time"))
                                                            {
                                                                string attribute_name = Convert.ToString(temp.Tables[0].Rows[j].ItemArray[0]);



                                                                //==================

                                                                DataTable attr1 = new DataTable();
                                                                String query = "select * from TB_ATTRIBUTE where ATTRIBUTE_NAME = '" + attribute_name + "'";
                                                                SqlCommand cmd3 = new SqlCommand(query, sqlConnection);
                                                                sqlConnection.Open();
                                                                sqlDataAdapter = new SqlDataAdapter(cmd3);

                                                                sqlDataAdapter.Fill(attr1);
                                                                var selattributeDataFormat = attr1.AsEnumerable().ToList();
                                                                sqlConnection.Close();


                                                                String getAttributeDataFormat = string.Empty;
                                                                if (selattributeDataFormat != null)
                                                                    getAttributeDataFormat = attr1.Rows[0]["ATTRIBUTE_NAME"].ToString();

                                                                if (!string.IsNullOrEmpty(getAttributeDataFormat))
                                                                {
                                                                    if (getAttributeDataFormat.StartsWith("(?=\\d\\d(\\-|\\/|\\.)\\d\\d(\\-|\\/|\\.)\\d{4})(?=.{0}(?:0[1-9]|[12]\\d|3[01]))(?=.{3}"))
                                                                    {
                                                                        string tempdate = Convert.ToString(dr[objDataSet.Tables[1].Columns[i]]);
                                                                        dr[objDataSet.Tables[1].Columns[i]] = tempdate.Substring(3, 2) + "/" + tempdate.Substring(0, 2) + "/" + tempdate.Substring(6, 4);
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    //}//
                                }
                            }
                        }
                        _logger.Info("SUB Product Export End");
                        /***************** SUB Product Export End *********************/
                        //DsPreview = functionCall();
                        // DataRow[] delRow = objDataSet.Tables[0].Select("len(CATEGORY_ID) >1");
                        int remColumn = 0;
                        if (objDataSet.Tables.Count > 2 && attrBy != "Row")
                        {
                            if (objDataSet.Tables[3].Columns.Contains("DISPLAY_TABLE_HEADER"))
                                objDataSet.Tables[3].Columns.Remove("DISPLAY_TABLE_HEADER");
                        }
                        else if (objDataSet.Tables.Count > 1 && attrBy == "Row")
                        {
                            if (objDataSet.Tables[2].Columns.Contains("DISPLAY_TABLE_HEADER"))
                                objDataSet.Tables[2].Columns.Remove("DISPLAY_TABLE_HEADER");
                        }

                        if (attrBy == "Row" && remColumn == 0)
                        {
                            if (objDataSet.Tables.Count > 2)
                            {
                                // objDataSet.Tables[2].Columns.Remove("ATTRIBUTE_ID");
                                // objDataSet.Tables[2].Columns.Remove("ATTRIBUTE_NAME");
                                // objDataSet.Tables[2].Columns.Remove("STRING_VALUE");
                                objDataSet.Tables[4].Columns.Remove("NUMERIC_VALUE");
                                objDataSet.Tables[4].Columns.Remove("OBJECT_NAME");
                                objDataSet.Tables[4].Columns.Remove("OBJECT_TYPE");
                            }
                        }
                        var flatFamily = objcustomExport.FamilyFilterFlatTable(objDataSet, filter, catalogid);
                        var finalDs = objcustomExport.ProductFilterFlatTable(flatFamily, filter, catalogid);

                        if (attrBy == "Row")
                        {
                            string ssidrow = Guid.NewGuid().ToString();
                            foreach (var item in selectedAttributes)
                            {
                                string attrName = item["ATTRIBUTE_NAME"].ToString();
                                string attrValue = item["STRING_VALUE"].ToString();
                                DataTable tblFiltered = new DataTable();
                                using (var conn = new SqlConnection(sqlConn))
                                {
                                    conn.Open();
                                    string _sqlString = null;
                                    _sqlString = "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''[##EXPROWFILTER" + ssidrow + "]'')BEGIN DROP TABLE [##EXPROWFILTER" + ssidrow + "] END')";
                                    SqlCommand _DBCommand = new SqlCommand(_sqlString, conn);
                                    _DBCommand.ExecuteNonQuery();

                                    if (finalDs.Tables.Count > 2)
                                    {
                                        _sqlString = CreateTable("[##EXPROWFILTER" + ssidrow + "]", finalDs.Tables[3]);
                                        SqlCommand _DBCommandn = new SqlCommand(_sqlString, conn);
                                        _DBCommandn.ExecuteNonQuery();

                                        var bulkCopy_cp = new SqlBulkCopy(conn)
                                        {
                                            DestinationTableName = "[##EXPROWFILTER" + ssidrow + "]"
                                        };
                                        bulkCopy_cp.WriteToServer(finalDs.Tables[3]);
                                    }
                                    else
                                    {
                                        _sqlString = CreateTable("[##EXPROWFILTER" + ssidrow + "]", finalDs.Tables[0]);
                                        SqlCommand _DBCommandn = new SqlCommand(_sqlString, conn);
                                        _DBCommandn.ExecuteNonQuery();

                                        var bulkCopy_cp = new SqlBulkCopy(conn)
                                        {
                                            DestinationTableName = "[##EXPROWFILTER" + ssidrow + "]"
                                        };
                                        bulkCopy_cp.WriteToServer(finalDs.Tables[0]);
                                    }
                                    DataSet rowfilter = new DataSet();
                                    var objSqlDataAdapterexp = new SqlDataAdapter("select * from [##EXPROWFILTER" + ssidrow + "] where ATTRIBUTE_NAME in ('" + attrName + "')  and STRING_VALUE in (" + attrValue + ") ", conn);
                                    objSqlDataAdapterexp.Fill(rowfilter);
                                    attributedataset.Merge(rowfilter);
                                }
                            }
                            if (attributedataset.Tables.Count > 0 && attributedataset.Tables[0].Rows.Count > 0)
                            {
                                finalDs = new DataSet();
                                finalDs = attributedataset;
                            }
                            if (!string.IsNullOrEmpty(selfamily_id))
                            {
                                using (var conn = new SqlConnection(sqlConn))
                                {
                                    conn.Open();
                                    string _sqlString = null;
                                    _sqlString = "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''[##ROWAMFILTER" + ssidrow + "]'')BEGIN DROP TABLE [##ROWAMFILTER" + ssidrow + "] END')";
                                    SqlCommand _DBCommand = new SqlCommand(_sqlString, conn);
                                    _DBCommand.ExecuteNonQuery();
                                    if (finalDs.Tables.Count > 0)
                                    {
                                        _sqlString = CreateTable("[##ROWAMFILTER" + ssidrow + "]", finalDs.Tables[0]);
                                        SqlCommand _DBCommandn = new SqlCommand(_sqlString, conn);
                                        _DBCommandn.ExecuteNonQuery();
                                        var bulkCopy_cp = new SqlBulkCopy(conn)
                                        {
                                            DestinationTableName = "[##ROWAMFILTER" + ssidrow + "]"
                                        };
                                        bulkCopy_cp.WriteToServer(finalDs.Tables[0]);
                                        finalDs = new DataSet();
                                        var objSqlDataAdapterexp = new SqlDataAdapter("select * from [##ROWAMFILTER" + ssidrow + "] where FAMILY_ID in (" + selfamily_id + ")   ", conn);
                                        objSqlDataAdapterexp.Fill(finalDs);
                                    }
                                }
                            }
                            DataTable dt_test1 = new DataTable();
                            DataTable dt_test2 = new DataTable();
                            if (finalDs.Tables.Count > 3 && selectedAttributes.Count > 0)
                            {
                                dt_test1 = attributedataset.Tables[0].Copy();
                                dt_test2 = finalDs.Tables[4].Copy();
                                DataTable tablerem = finalDs.Tables[3];
                                DataTable tablerem1 = finalDs.Tables[4];
                                finalDs.Tables.Remove(tablerem);
                                finalDs.Tables.Remove(tablerem1);
                                finalDs.Tables.Add(dt_test1);
                                finalDs.Tables.Add(dt_test2);
                                finalDs.Tables[3].TableName = "Joined";
                            }
                        }
                        DataSet mainprod = new DataSet();
                        using (var conn = new SqlConnection(sqlConn))
                        {
                            var datatablefortemp = new DataTable();
                            if (finalDs.Tables.Count == 5)
                            {

                                datatablefortemp = finalDs.Tables[3];
                            }
                            else
                            {
                                datatablefortemp = finalDs.Tables[0];
                            }

                            string ssid = Guid.NewGuid().ToString();
                            conn.Open();

                            string _sqlString = null;
                            _sqlString = "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''[##EXPORTSORT" + ssid + "]'')BEGIN DROP TABLE [##EXPORTSORT" + ssid + "] END')";
                            SqlCommand _DBCommand = new SqlCommand(_sqlString, conn);
                            _DBCommand.ExecuteNonQuery();

                            _sqlString = CreateTable("[##EXPORTSORT" + ssid + "]", datatablefortemp);
                            SqlCommand _DBCommandn = new SqlCommand(_sqlString, conn);
                            _DBCommandn.ExecuteNonQuery();

                            var bulkCopy_cp = new SqlBulkCopy(conn)
                            {
                                DestinationTableName = "[##EXPORTSORT" + ssid + "]"
                            };
                            bulkCopy_cp.WriteToServer(datatablefortemp);

                            DataTable sub = new DataTable();
                            if (finalDs.Tables.Count > 0)
                            {
                                var datatableforsub = new DataTable();
                                if (finalDs.Tables.Count == 5)
                                {
                                    datatableforsub = finalDs.Tables[4];
                                }
                                else if (finalDs.Tables.Count == 2)
                                {
                                    datatableforsub = finalDs.Tables[1];
                                }
                                else
                                {
                                    datatableforsub = finalDs.Tables[0];
                                }
                                if (datarange.Tables.Count > 0 && datarange.Tables[0].Rows.Count > 0)
                                {
                                    _sqlString = "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''[##Filtertable" + ssid + "]'')BEGIN DROP TABLE [##Filtertable" + ssid + "] END')";
                                    SqlCommand _DBCommandnew1 = new SqlCommand(_sqlString, conn);
                                    _DBCommandnew1.ExecuteNonQuery();

                                    _sqlString = CreateTable("[##Filtertable" + ssid + "]", datarange.Tables[0]);
                                    SqlCommand _DBCommandnew2 = new SqlCommand(_sqlString, conn);
                                    _DBCommandnew2.ExecuteNonQuery();

                                    var bulkCopy_cp_new = new SqlBulkCopy(conn)
                                    {
                                        DestinationTableName = "[##Filtertable" + ssid + "]"
                                    };
                                    bulkCopy_cp_new.WriteToServer(datarange.Tables[0]);

                                    var objSqlDataAdapterexp = new SqlDataAdapter("select TC.* from [##EXPORTSORT" + ssid + "] TC  left join   TB_CATALOG_FAMILY TB ON TB.FAMILY_id=TC.FAMILY_id AND TB.CATALOG_ID=TC.CATALOG_iD join   [##Filtertable" + ssid + "] cc on cc.product_id = tc.product_id  ORDER BY TB.SORT_ORDER ", conn);
                                    objSqlDataAdapterexp.Fill(mainprod);
                                }
                                else
                                {
                                    var objSqlDataAdapterexp = new SqlDataAdapter("select TC.* from [##EXPORTSORT" + ssid + "] TC  left join   TB_CATALOG_FAMILY TB ON TB.FAMILY_id=TC.FAMILY_id AND TB.CATALOG_ID=TC.CATALOG_iD  ORDER BY TB.SORT_ORDER ", conn);
                                    objSqlDataAdapterexp.Fill(mainprod);
                                }
                                sub = datatableforsub.Copy();

                                finalDs = new DataSet();
                                finalDs.Merge(mainprod);
                                //if (exporsubproducts == "Yes")
                                if (EnableSubProduct == true)
                                {
                                    sub.TableName = "Table1";
                                    finalDs.Tables.Add(sub);
                                }
                            }
                        }
                        if (attrBy == "Row")
                        {
                            ExprotRowRemove(finalDs, model, customerid);
                            DataSet dsFamilyExport = new DataSet();
                            dsFamilyExport.Tables.Add(familyExport);
                            ExprotRowRemove(dsFamilyExport, model, customerid);
                        }
                        else
                        {
                            ExprotColumnRemove(finalDs, model, exportids, customerid, customerSetting);
                            DataSet dsFamilyExport = new DataSet();
                            dsFamilyExport.Tables.Add(familyExport);
                            ExprotColumnRemove(dsFamilyExport, model, exportids, customerid, customerSetting);
                        }
                        string[] idColumns = { "CATALOG_ID", "CATEGORY_ID", "SUBCATID_L1", "FAMILY_ID", "SUBFAMILY_ID", "PRODUCT_ID", "SUBPRODUCT_ID" };
                        if (customerSetting == false)
                        {
                            foreach (var idColumn in idColumns)
                            {
                                if (finalDs.Tables[0].Columns.Contains(idColumn))
                                    finalDs.Tables[0].Columns.Remove(idColumn);
                            }
                        }
                        string[] hierarchyFamColumns = { "CATALOG_ID", "CATALOG_NAME", "CATEGORY_ID", "CATEGORY_NAME", "CATALOG_VERSION", "CATALOG_DESCRIPTION", "CATALOG_FAMILY_FILTERS", "CATALOG_PRODUCT_FILTERS", "CATEGORY_CUSTOM_NUM_FIELD1", "CATEGORY_CUSTOM_NUM_FIELD2", "CATEGORY_CUSTOM_NUM_FIELD3", "CATEGORY_CUSTOM_TEXT_FIELD1", "CATEGORY_CUSTOM_TEXT_FIELD2", "CATEGORY_CUSTOM_TEXT_FIELD3", "CATEGORY_IMAGE_FILE", "CATEGORY_IMAGE_FILE2", "CATEGORY_IMAGE_NAME", "CATEGORY_IMAGE_NAME2", "CATEGORY_IMAGE_TYPE", "CATEGORY_IMAGE_TYPE2", "CATEGORY_SHORT_DESC" };
                        string[] hierarchyColumns = { "CATALOG_ID", "CATALOG_NAME", "CATEGORY_ID", "CATEGORY_NAME", "SUBCATID_L1", "SUBCATNAME_L1", "SUBCATID_L2", "SUBCATNAME_L2", "SUBCATID_L3", "SUBCATNAME_L3", "SUBCATID_L4", "SUBCATNAME_L4", "SUBCATID_L5", "SUBCATNAME_L5", "SUBCATID_L6", "SUBCATNAME_L6", "FAMILY_ID", "FAMILY_NAME", "SUBFAMILY_ID", "SUBFAMILY_NAME", "PRODUCT_ID", "SUBPRODUCT_ID", "CATALOG_ITEM_NO" };


                        string[] exportidColumns = { "CATALOG_ID", "CATEGORY_ID", "SUBCATID_L1", "SUBCATID_L2", "SUBCATID_L3", "SUBCATID_L4", "SUBCATID_L5", "SUBCATID_L6", "FAMILY_ID", "SUBFAMILY_ID", "PRODUCT_ID", "SUBPRODUCT_ID" };





                        string filepath = System.Reflection.Assembly.GetExecutingAssembly().Location;
                        filepath = filepath + "~/Content/" + projectName + "";
                        // string filepath = Assembly.GetExecutingAssembly().Location("~/Content/" + projectName + "");
                        //System.Reflection.Assembly.GetExecutingAssembly().Location

                        string format = string.Empty;
                        if (finalDs.Tables.Count > 0)
                        {
                            if (exportids == "No")
                            {
                                foreach (var items in exportidColumns)
                                {
                                    if (finalDs.Tables[0].Columns.Contains(items))
                                        finalDs.Tables[0].Columns.Remove(items);

                                    if (finalDs.Tables.Count >= 2)
                                    {
                                        if (finalDs.Tables[1].Columns.Contains(items))
                                            finalDs.Tables[1].Columns.Remove(items);
                                    }
                                }
                            }

                            DataTable removetable = new DataTable();
                            DataTable removetablesub = new DataTable();

                            var CSVtable = new DataTable();
                            var suproduct = new DataTable();
                            if (finalDs.Tables.Count > 3)
                            {
                                CSVtable = finalDs.Tables[3];
                                suproduct = finalDs.Tables[4];
                                removetable = CSVtable;
                            }
                            else
                            {
                                CSVtable = finalDs.Tables[0];
                                if (finalDs.Tables.Count > 1)
                                {
                                    suproduct = finalDs.Tables[1];
                                    foreach (DataRow item in dispaytable.Rows)
                                    {
                                        var attributeName = item["ATTRIBUTE_NAME"].ToString();
                                        var display_AttributeName = item["DISPLAY_NAME"].ToString();
                                        if (suproduct.Columns.Contains(attributeName))
                                        {
                                            suproduct.Columns[attributeName].ColumnName = display_AttributeName;
                                        }
                                    }
                                }
                                removetable = CSVtable;
                            }

                            if (hierarchy == "No")
                            {
                                _logger.Info("For Removing main product hierarchy");
                                //-------------For Removing main product hierarchy --------------------------//
                                foreach (var items in hierarchyColumns)
                                {
                                    if (CSVtable.Columns.Contains(items))
                                        CSVtable.Columns.Remove(items);
                                }
                                foreach (var items in hierarchyFamColumns)
                                {

                                    if (familyExport.Columns.Contains(items) || familyExport.Columns.Contains("SUBCAT") || familyExport.Columns.Contains("CATEGORY_PUBLISH2"))
                                        familyExport.Columns.Remove(items);
                                }
                                var ColumnCount = familyExport.Columns.Count;
                                for (int count = 0; count < ColumnCount; count++)
                                {
                                    if (familyExport.Columns[count].ColumnName.ToUpper().Contains("SUBCAT") || familyExport.Columns[count].ColumnName.ToUpper().Contains("CATEGORY_PUBLISH2"))
                                    {
                                        familyExport.Columns.RemoveAt(count);
                                        ColumnCount--;
                                        count--;
                                    }
                                }
                                removetable = CSVtable;
                                foreach (var item in removetable.Columns)
                                {
                                    //===================

                                    DataTable attr3 = new DataTable();
                                    String query = "select ATTRIBUTE_NAME from TB_ATTRIBUTE where ATTRIBUTE_ID = '1'";
                                    SqlCommand cmd3 = new SqlCommand(query, sqlConnection);
                                    sqlConnection.Open();
                                    string attrname = (string)cmd3.ExecuteScalar();
                                    sqlConnection.Close();




                                    //   string attrname = _dbcontext.TB_ATTRIBUTE.Where(x => x.ATTRIBUTE_ID == 1).Select(a => a.ATTRIBUTE_NAME).FirstOrDefault().ToString();


                                    if (attrname == item.ToString())
                                    {
                                        for (int i = removetable.Rows.Count - 1; i >= 0; i--)
                                        {
                                            DataRow dr = removetable.Rows[i];
                                            if (dr[attrname] == DBNull.Value)
                                            {
                                                dr.Delete();
                                            }
                                        }
                                    }
                                }
                                removetable.AcceptChanges();
                                _logger.Info("Removing subproduct product hierarchy");
                                //-------------For Removing subproduct product hierarchy --------------------------//
                                if (finalDs.Tables.Count > 1)
                                {
                                    foreach (var items in hierarchyColumns)
                                    {
                                        if (suproduct.Columns.Contains(items))
                                            suproduct.Columns.Remove(items);
                                    }
                                    removetablesub = suproduct;
                                    foreach (var item in removetablesub.Columns)
                                    {
                                        string attrname = "SUBCATALOG_ITEM_NO";
                                        if (attrname == item.ToString())
                                        {
                                            for (int i = removetablesub.Rows.Count - 1; i >= 0; i--)
                                            {
                                                DataRow dr = removetablesub.Rows[i];
                                                if (dr[attrname] == DBNull.Value)
                                                {
                                                    dr.Delete();
                                                }
                                            }
                                        }
                                    }
                                    removetablesub.AcceptChanges();
                                    suproduct = new DataTable();
                                    suproduct = removetablesub;
                                }
                                CSVtable = new DataTable();

                                foreach (DataRow item in dispaytable.Rows)
                                {
                                    var attributeName = item["ATTRIBUTE_NAME"].ToString();
                                    var display_AttributeName = item["DISPLAY_NAME"].ToString();
                                    if (removetable.Columns.Contains(attributeName))
                                    {
                                        // objDataSet.Columns[attributeName].SetOrdinal(sortOrder - 1);
                                        removetable.Columns[attributeName].ColumnName = display_AttributeName;
                                    }
                                }
                                foreach (DataRow item in dispaytable.Rows)
                                {
                                    var attributeName = item["ATTRIBUTE_NAME"].ToString();
                                    var display_AttributeName = item["DISPLAY_NAME"].ToString();
                                    if (familyExport.Columns.Contains(attributeName))
                                    {
                                        // objDataSet.Columns[attributeName].SetOrdinal(sortOrder - 1);
                                        familyExport.Columns[attributeName].ColumnName = display_AttributeName;
                                    }
                                }
                                CSVtable = removetable;
                            }
                            else
                            {
                                foreach (DataRow item in dispaytable.Rows)
                                {
                                    var attributeName = item["ATTRIBUTE_NAME"].ToString();
                                    var display_AttributeName = item["DISPLAY_NAME"].ToString();
                                    if (CSVtable.Columns.Contains(attributeName))
                                    {
                                        // objDataSet.Columns[attributeName].SetOrdinal(sortOrder - 1);
                                        CSVtable.Columns[attributeName].ColumnName = display_AttributeName;
                                    }
                                }

                                foreach (DataRow item in dispaytable.Rows)
                                {
                                    var attributeName = item["ATTRIBUTE_NAME"].ToString();
                                    var display_AttributeName = item["DISPLAY_NAME"].ToString();
                                    if (familyExport.Columns.Contains(attributeName))
                                    {
                                        // objDataSet.Columns[attributeName].SetOrdinal(sortOrder - 1);
                                        familyExport.Columns[attributeName].ColumnName = display_AttributeName;
                                    }
                                }
                            }

                            if (outputformat == "CSV")
                            {
                                format = filepath + ".csv";
                                CreateCSVFile(familyExport, format, false);
                                CreateCSVFile(removetable, format, false);
                                // if (exporsubproducts == "Yes")
                                if (EnableSubProduct == true)
                                    CreateCSVFile(suproduct, format, true);
                            }
                            else if (outputformat == "txt")
                            {
                                if (!String.IsNullOrEmpty(Delimiters))
                                {
                                    format = filepath + ".txt";
                                    CreateTextFile(familyExport, format, Delimiters, false);
                                    CreateTextFile(removetable, format, Delimiters, false);
                                    //if (exporsubproducts == "Yes")
                                    if (EnableSubProduct == true)
                                        CreateTextFile(suproduct, format, Delimiters, true);
                                }
                                else
                                {
                                    Delimiters = "\t";
                                    format = filepath + ".txt";
                                    CreateTextFile(familyExport, format, Delimiters, false);
                                    CreateTextFile(removetable, format, Delimiters, false);
                                    //if (exporsubproducts == "Yes")
                                    if (EnableSubProduct == true)
                                        CreateTextFile(suproduct, format, Delimiters, true);
                                }
                            }
                            else if (outputformat == "XML")
                            {
                                format = filepath + ".XML";
                                CreateXMLFile(familyExport, format, false, Convert.ToString(EnableSubProduct));//exporsubproducts);
                                CreateXMLFile(removetable, format, false, Convert.ToString(EnableSubProduct));//exporsubproducts);
                                //if (exporsubproducts == "Yes")
                                if (EnableSubProduct == true)
                                    CreateXMLFile(suproduct, format, true, Convert.ToString(EnableSubProduct));// exporsubproducts);
                            }
                            else if (outputformat == "XLS")
                            {
                                if (exportids == "No")
                                {
                                    foreach (var items in exportidColumns)
                                    {
                                        if (finalDs.Tables[0].Columns.Contains(items))
                                            finalDs.Tables[0].Columns.Remove(items);
                                    }
                                    foreach (var items in exportidColumns)
                                    {
                                        if (familyExport.Columns.Contains(items))
                                            familyExport.Columns.Remove(items);
                                    }
                                }
                                ExportDataSetToExcelFam(familyExport, fileName);

                                ExportDataSetToExcel(finalDs, fileName);
                            }
                        }
                        else
                        {
                            if (outputformat == "CSV")
                            {
                                format = filepath + ".csv";
                                CreateCSVFile(finalDs.Tables[0], format, false);
                                // if (exporsubproducts == "Yes")
                                if (EnableSubProduct == true)
                                {
                                    CreateCSVFile(finalDs.Tables[1], format, true);
                                }
                            }
                            else if (outputformat == "txt")
                            {
                                if (!String.IsNullOrEmpty(Delimiters))
                                {
                                    format = filepath + ".txt";
                                    CreateTextFile(finalDs.Tables[0], format, Delimiters, false);
                                    //if (exporsubproducts == "Yes")
                                    if (EnableSubProduct == true)
                                    {
                                        CreateTextFile(finalDs.Tables[1], format, Delimiters, true);
                                    }

                                }
                                else
                                {
                                    Delimiters = "\t";
                                    format = filepath + ".txt";
                                    CreateTextFile(finalDs.Tables[0], format, Delimiters, false);
                                    //if (exporsubproducts == "Yes")
                                    if (EnableSubProduct == true)
                                    {
                                        CreateTextFile(finalDs.Tables[1], format, Delimiters, true);
                                    }
                                }
                            }
                            else if (outputformat == "XML")
                            {
                                format = filepath + ".XML";
                                CreateXMLFile(finalDs.Tables[0], format, false, Convert.ToString(EnableSubProduct));//exporsubproducts);

                                //if (exporsubproducts == "Yes")
                                if (EnableSubProduct == true)
                                {
                                    CreateXMLFile(finalDs.Tables[1], format, true, Convert.ToString(EnableSubProduct));// exporsubproducts);
                                }
                            }
                            else if (outputformat == "XLS")
                            {
                                if (exportids == "No")
                                {
                                    foreach (var items in exportidColumns)
                                    {
                                        if (finalDs.Tables[0].Columns.Contains(items))
                                            finalDs.Tables[0].Columns.Remove(items);
                                    }
                                }
                                ExportDataSetToExcel(finalDs, fileName);
                            }
                        }



                        // ExportDataSetToExcel(finalDs, fileName);
                        var flatFamilySubProd = objcustomExport.FamilyFilterFlatTable(finalDs, filter, catalogid);
                        var finalDsSubProd = objcustomExport.ProductFilterFlatTable(flatFamily, filter, catalogid);
                        if (attrBy == "Row")
                            ExprotRowRemoveSub(finalDsSubProd, model, customerid);
                        else
                            ExprotColumnRemoveSub(finalDsSubProd, model, customerid, customerSetting);

                        if (attrBy == "Row")
                        {
                            attributedataset = null;
                            foreach (var item in selectedAttributes)
                            {
                                string attrName = item["ATTRIBUTE_NAME"].ToString();
                                string attrValue = item["STRING_VALUE"].ToString();
                                if (finalDs.Tables.Count > 3)
                                {
                                    if (finalDs.Tables[4].Rows.Count > 0)
                                    {
                                        DataTable tblFiltered = finalDs.Tables[4].AsEnumerable().Where(r => r.Field<string>("ATTRIBUTE_NAME") == attrName && r.Field<string>("STRING_VALUE") == attrValue).CopyToDataTable();
                                        var distinctds1 = tblFiltered.DefaultView.ToTable(true);
                                        if (attributedataset.Tables.Count > 0)
                                        {
                                            for (int i = 0; i < distinctds1.Rows.Count; i++)
                                                attributedataset.Tables[0].ImportRow(distinctds1.Rows[i]);
                                        }
                                        else
                                            attributedataset.Merge(distinctds1);
                                    }
                                }
                            }
                            DataTable dt_test1 = new DataTable();
                            DataTable dt_test2 = new DataTable();
                            if (finalDs.Tables.Count > 3 && selectedAttributes.Count > 0 && attributedataset != null)
                            {
                                dt_test1 = attributedataset.Tables[0].Copy();
                                //dt_test2 = finalDs.Tables[4].Copy();
                                DataTable tablerem = finalDs.Tables[4];
                                //DataTable tablerem1 = finalDs.Tables[4];
                                finalDs.Tables.Remove(tablerem);
                                //finalDs.Tables.Remove(tablerem1);
                                finalDs.Tables.Add(dt_test1);
                                //finalDs.Tables.Add(dt_test2);
                            }
                        }
                        //string[] idColumns = { "CATALOG_ID", "CATEGORY_ID", "FAMILY_ID", "SUBFAMILY_ID", "PRODUCT_ID", "SUBPRODUCT_ID" };
                        if (customerSetting == false)
                        {
                            if (finalDsSubProd.Tables.Count == 2)
                            {
                                foreach (var idColumn in idColumns)
                                {
                                    if (finalDsSubProd.Tables[1].Columns.Contains(idColumn))
                                        finalDsSubProd.Tables[1].Columns.Remove(idColumn);
                                }
                            }
                        }
                        string[] hierarchyColumnssub = { "CATALOG_ID", "CATALOG_NAME", "CATEGORY_ID", "CATEGORY_NAME", "SUBCATID_L1", "SUBCATNAME_L1", "SUBCATID_L2", "SUBCATNAME_L2", "SUBCATID_L3", "SUBCATNAME_L3", "SUBCATID_L4", "SUBCATNAME_L4", "SUBCATID_L5", "SUBCATNAME_L5", "SUBCATID_L6", "SUBCATNAME_L6", "FAMILY_ID", "FAMILY_NAME", "SUBFAMILY_ID", "SUBFAMILY_NAME", "PRODUCT_ID", "SUBPRODUCT_ID", "CATALOG_ITEM_NO" };
                        if (exportids == "No")
                        {
                            if (finalDsSubProd.Tables.Count > 1)
                            {
                                foreach (var items in exportidColumns)
                                {
                                    if (finalDsSubProd.Tables[1].Columns.Contains(items))
                                        finalDsSubProd.Tables[1].Columns.Remove(items);
                                }
                            }
                        }
                        if (hierarchy == "No")
                        {
                            var tablesub = new DataTable();
                            if (finalDsSubProd.Tables.Count > 1)
                            {
                                foreach (var items in hierarchyColumnssub)
                                {
                                    if (finalDsSubProd.Tables[1].Columns.Contains(items))
                                        finalDsSubProd.Tables[1].Columns.Remove(items);
                                }
                                tablesub = finalDsSubProd.Tables[1];
                                foreach (var item in tablesub.Columns)
                                {
                                    string attrname = "SUBCATALOG_ITEM_NO";
                                    if (attrname == item.ToString())
                                    {
                                        for (int i = tablesub.Rows.Count - 1; i >= 0; i--)
                                        {
                                            DataRow dr = tablesub.Rows[i];
                                            if (dr[attrname] == DBNull.Value)
                                                dr.Delete();
                                        }
                                    }
                                }
                                tablesub.AcceptChanges();
                            }
                        }
                        if (finalDsSubProd.Tables.Count > 1)
                        {
                            foreach (DataRow item in dispaytable.Rows)
                            {
                                var attributeName = item["ATTRIBUTE_NAME"].ToString();
                                var display_AttributeName = item["DISPLAY_NAME"].ToString();
                                if (finalDsSubProd.Tables[1].Columns.Contains(attributeName))
                                    finalDsSubProd.Tables[1].Columns[attributeName].ColumnName = display_AttributeName;
                            }
                        }

                        ExportDataSetToExcelSubProduct(finalDsSubProd, fileName);
                        //if (finalDsSubProd.Tables.Count > 0 && attrBy != "Row")
                        //if (finalDsSubProd.Tables[1].Rows.Count > 0 && attrBy != "Row")
                        //{
                        //    DataColumnCollection columns = objDataSet.Tables[1].Columns;
                        //    if (columns.Contains("Item #"))
                        //    {
                        //        objDataSet.Tables[1].Columns.Remove("Item #");
                        //    }
                        //}
                        // ExportDataSetToMSExcel(finalDs, fileName);
                        //if ((finalDS.Tables[0].Rows[0]["Product_id"].ToString() == "") && (aviAttrr.ToString() != ""))
                        //{
                        // finalDS.Tables[0].Rows.RemoveAt(0);
                        //}
                        //  expData.CustomExport(finalDS, FileName.Trim(), Utilities.Export.ExportType.Excel);
                        //wb.Worksheets[0].Cells.Replace("find", "replace");
                    }
                    catch (Exception objexception)
                    {
                        _logger.Info("Error");
                       // Logger.Error("Error at Export : ExportXls", objexception);
                        //return Request.CreateResponse(HttpStatusCode.InternalServerError);
                    }
                    #endregion

                    //Batch Process End- Home API=================================================================================================================================================


                }
                _logger.Info("End Export Process");
                //End Export Process

                //Download
                program.Download(ExportTable, ExportTableFAM, ExportTableSubProduct, batchId, ExportId);
            }
            catch (Exception ex)
            {
                _logger.Info("Error1");
            }


        }

        #region CommonFunctions

       //Use Predefined Code.
       public static string CreateTable(string tableName, DataTable table)
        {
            _logger.Info("CreateTable");
            try
            {
                string sqlsc = "CREATE TABLE " + tableName + "(";
                for (int i = 0; i < table.Columns.Count; i++)
                {
                    if (!table.Columns[i].ColumnName.Contains('['))
                    {
                        sqlsc += "\n [" + table.Columns[i].ColumnName + "] ";
                    }
                    else
                    {
                        sqlsc += "\n" + table.Columns[i].ColumnName + "";
                    }
                    if (table.Columns[i].DataType.ToString().Contains("System.String"))
                        sqlsc += "nvarchar(max) ";
                    else
                        sqlsc += "int";
                    sqlsc += ",";
                }
                return sqlsc.Substring(0, sqlsc.Length - 1) + ")";
            }
            catch (Exception ex)
            {
                _logger.Info("Error in CreateTable");
                return null;
            }
        }


       public void ExprotColumnRemove(DataSet finalDs, JArray functionAlloweditems, string exportwithids,int customerid,bool customerSetting)
       {
           _logger.Info("ExprotColumnRemove");
           try
           {
               var removecolumnname = new List<string>();
               var removecolumnnames = new List<string>();

               var functionAlloweditem = (functionAlloweditems[1]).Select(x => x).ToList();
               foreach (var item in functionAlloweditem)
               {
                   string attributeName = Convert.ToString(item["ATTRIBUTE_NAME"]);
                   removecolumnname.Add(attributeName);
               }




             // var customerid = customerid;
              // var customerid = _dbcontext.Customer_User.FirstOrDefault(x => x.User_Name == User.Identity.Name);
               //var customerSetting =
               //    _dbcontext.Customer_Settings.Where(x => x.CustomerId == customerid.CustomerId)
               //        .Select(y => y.DisplayIDColumns)
               //        .ToList();
               //if (customerSetting[0] == false)
           //    var itemNoName = _dbcontext.Customers.Join(_dbcontext.Customer_User, c => c.CustomerId, cu => cu.CustomerId, (c, cu) => new { c, cu }).Where(x => x.cu.User_Name == User.Identity.Name).Select(y => y.c.CustomizeItemNo).FirstOrDefault();


               String query = "select c.CustomizeItemNo from Customers c join Customer_User cu on c.CustomerId = cu.CustomerId where cu.User_Name in ( select User_Name from Customer_User cu join EXPORTBATCHPROCESS Ex on cu.User_Name = Ex.CUSTOMER_NAME where cu.CustomerId = '" + customerid + "') ";

               SqlCommand cmd1 = new SqlCommand(query, sqlConnection);
               sqlConnection.Open();
               var itemNoName = Convert.ToString(cmd1.ExecuteScalar());

               sqlConnection.Close();


               if (string.IsNullOrEmpty(itemNoName))
               {
                   itemNoName = "ITEM#";
               }


               if (exportwithids == "No")
               {
                   string[] idColumns = { "CATALOG_ID", "CATEGORY_ID", "FAMILY_ID", "SUBFAMILY_ID", "PRODUCT_ID", "SUBPRODUCT_ID" };

                   foreach (var item in idColumns)
                   {
                       string attributeName = Convert.ToString(item);
                       removecolumnname.Add(attributeName);
                   }
               }
               foreach (DataTable table in finalDs.Tables)
               {
                   foreach (DataColumn finaldstablecolumns in table.Columns)
                   {
                       bool functionAlloweditemattrname = removecolumnname.Contains(finaldstablecolumns.ColumnName);
                       //if (!functionAlloweditemattrname && !finaldstablecolumns.ColumnName.Contains("SUBCAT"))
                       //{
                       if (!functionAlloweditemattrname && !finaldstablecolumns.ColumnName.Contains("SUBCAT") && !finaldstablecolumns.ColumnName.Contains("CATALOG_ITEM_NO") && !finaldstablecolumns.ColumnName.Contains("SUBCATALOG_ITEM_NO") && !finaldstablecolumns.ColumnName.Contains("SUBPRODUCT_ID") && !finaldstablecolumns.ColumnName.Contains("PUBLISH2") && !finaldstablecolumns.ColumnName.Contains(itemNoName))
                       {
                           removecolumnnames.Add(finaldstablecolumns.ColumnName);
                           //   table.Columns.Remove(finaldstablecolumns);
                       }
                   }
               }
               foreach (var removecolumns in removecolumnnames)
               {
                   foreach (DataTable table in finalDs.Tables)
                   {
                       if (table.Columns.Contains(removecolumns) && removecolumns != "FAMILYID")
                       {
                           table.Columns.Remove(removecolumns);
                       }
                   }
               }
           }
           catch (Exception objexception)
           {
               _logger.Info("Error in ExprotColumnRemove");
               // Logger.Error("Error at Export : ExportXls", objexception);
               //return Request.CreateResponse(HttpStatusCode.InternalServerError);
           }
       }
       public void ExprotColumnRemoveSub(DataSet finalDs, JArray functionAlloweditems, int customerid, bool customerSetting)
       {
           _logger.Info("ExprotColumnRemoveSub");
           try
           {
               var removecolumnname = new List<string>();
               var removecolumnnames = new List<string>();

               var functionAlloweditem = (functionAlloweditems[1]).Select(x => x).ToList();
               foreach (var item in functionAlloweditem)
               {
                   string attributeName = Convert.ToString(item["ATTRIBUTE_NAME"]);
                   removecolumnname.Add(attributeName);
               }
              // var customerid = _dbcontext.Customer_User.FirstOrDefault(x => x.User_Name == User.Identity.Name);
               //var customerSetting =
               //    _dbcontext.Customer_Settings.Where(x => x.CustomerId == customerid.CustomerId)
               //        .Select(y => y.DisplayIDColumns)
               //        .ToList();
              // var itemNoName = _dbcontext.Customers.Join(_dbcontext.Customer_User, c => c.CustomerId, cu => cu.CustomerId, (c, cu) => new { c, cu }).Where(x => x.cu.User_Name == User.Identity.Name).Select(y => y.c.CustomizeSubItemNo).FirstOrDefault();

               String query = "select c.CustomizeItemNo from Customers c join Customer_User cu on c.CustomerId = cu.CustomerId where cu.User_Name in ( select User_Name from Customer_User cu join EXPORTBATCHPROCESS Ex on cu.User_Name = Ex.CUSTOMER_NAME where cu.CustomerId = '" + customerid + "') ";

               SqlCommand cmd1 = new SqlCommand(query, sqlConnection);
               sqlConnection.Open();
               var itemNoName = Convert.ToString(cmd1.ExecuteScalar());
               sqlConnection.Close();



               if (string.IsNullOrEmpty(itemNoName))
               {
                   itemNoName = "ITEM#";
               }


               if (customerSetting == true)
               {
                   string[] idColumns = { "CATALOG_ID", "CATEGORY_ID", "FAMILY_ID", "SUBFAMILY_ID", "PRODUCT_ID", "SUBPRODUCT_ID" };

                   foreach (var item in idColumns)
                   {
                       string attributeName = Convert.ToString(item);
                       removecolumnname.Add(attributeName);
                   }
               }
               foreach (DataTable table in finalDs.Tables)
               {
                   foreach (DataColumn finaldstablecolumns in table.Columns)
                   {
                       bool functionAlloweditemattrname = removecolumnname.Contains(finaldstablecolumns.ColumnName);
                       if (!functionAlloweditemattrname && !finaldstablecolumns.ColumnName.Contains("SUBCAT") && !finaldstablecolumns.ColumnName.Contains("CATALOG_ITEM_NO") && !finaldstablecolumns.ColumnName.Contains("SUBCATALOG_ITEM_NO") && !finaldstablecolumns.ColumnName.Contains("SUBPRODUCT_ID") && !finaldstablecolumns.ColumnName.Contains("PUBLISH2") && !finaldstablecolumns.ColumnName.Contains(itemNoName))
                       {
                           removecolumnnames.Add(finaldstablecolumns.ColumnName);
                       }
                       if (!functionAlloweditemattrname && (finaldstablecolumns.ColumnName.Contains("CATALOG_VERSION") || finaldstablecolumns.ColumnName.Contains("CATALOG_DESCRIPTION") || finaldstablecolumns.ColumnName.Contains("CATALOG_FAMILY_FILTERS") || finaldstablecolumns.ColumnName.Contains("CATALOG_PRODUCT_FILTERS")))
                       {
                           removecolumnnames.Add(finaldstablecolumns.ColumnName);
                       }
                       if (!functionAlloweditemattrname && finaldstablecolumns.ColumnName.StartsWith("CATEGORY_"))
                       {
                           removecolumnnames.Add(finaldstablecolumns.ColumnName);
                       }
                   }
               }
               foreach (var removecolumns in removecolumnnames)
               {
                   foreach (DataTable table in finalDs.Tables)
                   {

                       if (table.Columns.Contains(removecolumns) && removecolumns != "FAMILYID")
                       {
                           table.Columns.Remove(removecolumns);
                       }
                       // table.Columns.Remove(removecolumns);
                   }
               }
           }
           catch (Exception objexception)
           {
               _logger.Info("Error in  ExprotColumnRemoveSub");
               // Logger.Error("Error at Export : ExportXls", objexception);
               //return Request.CreateResponse(HttpStatusCode.InternalServerError);
           }
       }

       public void ExprotRowRemove(DataSet finalDs, JArray functionAlloweditems , int customerid)
       {
           _logger.Info("ExprotRowRemove");
           try
           {
               var removecolumnname = new List<string>();
               var removecolumnnames = new List<string>();

               var functionAlloweditem = (functionAlloweditems[1]).Select(x => x).ToList();
               foreach (var item in functionAlloweditem)
               {
                   string attributeName = Convert.ToString(item["ATTRIBUTE_NAME"]);
                   removecolumnname.Add(attributeName);
               }
               String query = "select c.CustomizeItemNo from Customers c join Customer_User cu on c.CustomerId = cu.CustomerId where cu.User_Name in ( select User_Name from Customer_User cu join EXPORTBATCHPROCESS Ex on cu.User_Name = Ex.CUSTOMER_NAME where cu.CustomerId = '" + customerid + "') ";

               SqlCommand cmd1 = new SqlCommand(query, sqlConnection);
               sqlConnection.Open();
               var itemNoName = Convert.ToString(cmd1.ExecuteScalar());
               sqlConnection.Close();


               if (string.IsNullOrEmpty(itemNoName))
               {
                   itemNoName = "ITEM#";
               }

              // var itemNoName = _dbcontext.Customers.Join(_dbcontext.Customer_User, c => c.CustomerId, cu => cu.CustomerId, (c, cu) => new { c, cu }).Where(x => x.cu.User_Name == User.Identity.Name).Select(y => y.c.CustomizeItemNo).FirstOrDefault();
               foreach (DataTable table in finalDs.Tables)
               {
                   foreach (DataColumn finaldstablecolumns in table.Columns)
                   {
                       bool functionAlloweditemattrname = removecolumnname.Contains(finaldstablecolumns.ColumnName);
                       //if (!functionAlloweditemattrname && !finaldstablecolumns.ColumnName.Contains("SUBCAT"))
                       //{
                       if (!functionAlloweditemattrname && !finaldstablecolumns.ColumnName.Contains("SUBCAT") && !finaldstablecolumns.ColumnName.Contains("CATALOG_ITEM_NO") && !finaldstablecolumns.ColumnName.Contains("SUBCATALOG_ITEM_NO") && !finaldstablecolumns.ColumnName.Contains("SUBPRODUCT_ID") && !finaldstablecolumns.ColumnName.Contains("PUBLISH2") && !finaldstablecolumns.ColumnName.Contains(itemNoName))
                       {
                           removecolumnnames.Add(finaldstablecolumns.ColumnName);
                           //   table.Columns.Remove(finaldstablecolumns);
                       }
                   }
               }
               foreach (var removecolumns in removecolumnnames)
               {
                   foreach (DataTable table in finalDs.Tables)
                   {
                       if (table.Columns.Contains(removecolumns) && removecolumns != "FAMILYID" && removecolumns != "STRING_VALUE" && removecolumns != "ATTRIBUTE_NAME")
                       {
                           table.Columns.Remove(removecolumns);
                       }
                   }
               }
           }
           catch (Exception objexception)
           {
               _logger.Info("Error in ExprotRowRemove");
               // Logger.Error("Error at Export : ExportXls", objexception);
               //return Request.CreateResponse(HttpStatusCode.InternalServerError);
           }
       }


       public void ExprotRowRemoveSub(DataSet finalDs, JArray functionAlloweditems, int customerid)
       {
           _logger.Info("ExprotRowRemoveSub");
           try
           {
               var removecolumnname = new List<string>();
               var removecolumnnames = new List<string>();

               var functionAlloweditem = (functionAlloweditems[1]).Select(x => x).ToList();
               foreach (var item in functionAlloweditem)
               {
                   string attributeName = Convert.ToString(item["ATTRIBUTE_NAME"]);
                   removecolumnname.Add(attributeName);
               }


               String query = "select c.CustomizeItemNo from Customers c join Customer_User cu on c.CustomerId = cu.CustomerId where cu.User_Name in ( select User_Name from Customer_User cu join EXPORTBATCHPROCESS Ex on cu.User_Name = Ex.CUSTOMER_NAME where cu.CustomerId = '" + customerid + "') ";

               SqlCommand cmd1 = new SqlCommand(query, sqlConnection);
               sqlConnection.Open();
               var itemNoName = Convert.ToString(cmd1.ExecuteScalar());
               sqlConnection.Close();

               if (string.IsNullOrEmpty(itemNoName))
               {
                   itemNoName = "ITEM#";
               }



              // var itemNoName = _dbcontext.Customers.Join(_dbcontext.Customer_User, c => c.CustomerId, cu => cu.CustomerId, (c, cu) => new { c, cu }).Where(x => x.cu.User_Name == User.Identity.Name).Select(y => y.c.CustomizeSubItemNo).FirstOrDefault();
               foreach (DataTable table in finalDs.Tables)
               {
                   foreach (DataColumn finaldstablecolumns in table.Columns)
                   {
                       bool functionAlloweditemattrname = removecolumnname.Contains(finaldstablecolumns.ColumnName);
                       if (!functionAlloweditemattrname && !finaldstablecolumns.ColumnName.Contains("SUBCAT") && !finaldstablecolumns.ColumnName.Contains("CATALOG_ITEM_NO") && !finaldstablecolumns.ColumnName.Contains("SUBCATALOG_ITEM_NO") && !finaldstablecolumns.ColumnName.Contains("SUBPRODUCT_ID") && !finaldstablecolumns.ColumnName.Contains("PUBLISH2") && !finaldstablecolumns.ColumnName.Contains(itemNoName))
                       {
                           removecolumnnames.Add(finaldstablecolumns.ColumnName);
                       }
                       if (!functionAlloweditemattrname && (finaldstablecolumns.ColumnName.Contains("CATALOG_VERSION") || finaldstablecolumns.ColumnName.Contains("CATALOG_DESCRIPTION") || finaldstablecolumns.ColumnName.Contains("CATALOG_FAMILY_FILTERS") || finaldstablecolumns.ColumnName.Contains("CATALOG_PRODUCT_FILTERS")))
                       {
                           removecolumnnames.Add(finaldstablecolumns.ColumnName);
                       }
                       if (!functionAlloweditemattrname && finaldstablecolumns.ColumnName.StartsWith("CATEGORY_"))
                       {
                           removecolumnnames.Add(finaldstablecolumns.ColumnName);
                       }
                   }
               }
               foreach (var removecolumns in removecolumnnames)
               {
                   foreach (DataTable table in finalDs.Tables)
                   {

                       if (table.Columns.Contains(removecolumns) && removecolumns != "FAMILYID" && removecolumns != "STRING_VALUE" && removecolumns != "ATTRIBUTE_NAME")
                       {
                           table.Columns.Remove(removecolumns);
                       }
                       // table.Columns.Remove(removecolumns);
                   }
               }
           }
           catch (Exception objexception)
           {
               _logger.Info("Error in ExprotRowRemoveSub");
               // Logger.Error("Error at Export : ExportXls", objexception);
               //return Request.CreateResponse(HttpStatusCode.InternalServerError);
           }
       }

       public void ExportDataSetToCSV(DataSet finalDs, string fileName)
       {
           try
           {
               _logger.Info("ExportDataSetToCSV");
               if (finalDs.Tables[0].Columns.Contains("FAMILY_ID_1"))
               {
                   finalDs.Tables[0].Columns.Remove("FAMILY_ID_1");
               }
               if (finalDs.Tables[0].Columns.Contains("FAMILY_FOOT_NOTES"))
               {
                   finalDs.Tables[0].Columns.Remove("FAMILY_FOOT_NOTES");
               }
               if (finalDs.Tables[0].Columns.Contains("FAMILY_STATUS"))
               {
                   finalDs.Tables[0].Columns.Remove("FAMILY_STATUS");
               }

               if (fileName.Trim() != "")
               {
                  // object context;
                  // if (Request.Properties.TryGetValue("MS_HttpContext", out context))
                  // {
                      //// var httpContext = context as HttpContextBase;
                      // if (httpContext != null && httpContext.Session != null)
                     //  {
                           if (finalDs.Tables[0].TableName == "ProductFamily")
                           {
                               ExportTable = finalDs.Tables[3];
                           }
                           else
                           {
                               ExportTable = finalDs.Tables[0];
                           }
                      // }
                 //  }

               }

           }
           catch (Exception objexception)
           {

               _logger.Info("Error in ExportDataSetToCSV");
               // Logger.Error("Error at Export : ExportXls", objexception);
               //return Request.CreateResponse(HttpStatusCode.InternalServerError);
           }
       }

       public void ExportDataSetToExcelFam(DataTable finalDs, string fileName)
       {
           _logger.Info("ExportDataSetToExcelFam");
           try
           {

               if (finalDs.Columns.Contains("FAMILY_ID_1"))
               {
                   finalDs.Columns.Remove("FAMILY_ID_1");
               }
               if (finalDs.Columns.Contains("FAMILY_FOOT_NOTES"))
               {
                   finalDs.Columns.Remove("FAMILY_FOOT_NOTES");
               }
               if (finalDs.Columns.Contains("FAMILY_STATUS"))
               {
                   finalDs.Columns.Remove("FAMILY_STATUS");
               }

               if (fileName.Trim() != "")
               {
                   //Creae an Excel application instance
                   // var excelApp = new Application();



                   // object misValue = System.Reflection.Missing.Value;

                   //Create an Excel workbook instance and open it from the predefined location

                   // Workbook excelWorkBook = excelApp.Workbooks.Add(misValue);
                   //foreach (DataTable table in finalDs.Tables)
                   //{
                   ////Add a new worksheet to workbook with the Datatable name
                   //Worksheet excelWorkSheet = excelWorkBook.Sheets.Add();
                   //excelWorkSheet.Name = table.TableName;

                   //for (int i = 1; i < table.Columns.Count + 1; i++)
                   //{
                   //    excelWorkSheet.Cells[1, i] = table.Columns[i - 1].ColumnName;
                   //}

                   //for (int j = 0; j < table.Rows.Count; j++)
                   //{
                   //    for (int k = 0; k < table.Columns.Count; k++)
                   //    {
                   //        excelWorkSheet.Cells[j + 2, k + 1] = table.Rows[j].ItemArray[k].ToString();
                   //    }
                   //}

                  // object context;
                  // if (Request.Properties.TryGetValue("MS_HttpContext", out context))
                  // {
                      // var httpContext = context as HttpContextBase;
                      // if (httpContext != null && httpContext.Session != null)
                     //  {
                           if (finalDs.TableName == "ProductFamily")
                           {
                               ExportTableFAM = finalDs;
                           }
                           else
                           {
                               ExportTableFAM = finalDs;
                           }
                     //  }
                 //  }
                   //var objExport = new RKLib.ExportData.Export();
                   //objExport.ExportDetails(table, RKLib.ExportData.Export.ExportFormat.Excel, fileName);
                   // }
                   // excelWorkBook.SaveAs(fileName, XlFileFormat.xlWorkbookNormal, misValue, misValue, misValue, misValue, XlSaveAsAccessMode.xlExclusive, misValue, misValue, misValue, misValue, misValue);

                   //  excelWorkBook.Close();
                   //   excelApp.Quit();


                   //   DownLoad(fileName);


               }
               //  expData.CustomExport(finalDS, FileName.Trim(), Utilities.Export.ExportType.Excel);
           }
           catch (Exception objexception)
           {
               _logger.Info("Error in ExportDataSetToExcelFam");
               // Logger.Error("Error at Export : ExportXls", objexception);
               //return Request.CreateResponse(HttpStatusCode.InternalServerError);
           }
       }

       public void ExportDataSetToExcel(DataSet finalDs, string fileName)
       {
           try
           {
               _logger.Info("ExportDataSetToExcel");

               if (finalDs.Tables[0].Columns.Contains("FAMILY_ID_1"))
               {
                   finalDs.Tables[0].Columns.Remove("FAMILY_ID_1");
               }
               if (finalDs.Tables[0].Columns.Contains("FAMILY_FOOT_NOTES"))
               {
                   finalDs.Tables[0].Columns.Remove("FAMILY_FOOT_NOTES");
               }
               if (finalDs.Tables[0].Columns.Contains("FAMILY_STATUS"))
               {
                   finalDs.Tables[0].Columns.Remove("FAMILY_STATUS");
               }

               if (fileName.Trim() != "")
               {
                   //Creae an Excel application instance
                   // var excelApp = new Application();



                   // object misValue = System.Reflection.Missing.Value;

                   //Create an Excel workbook instance and open it from the predefined location

                   // Workbook excelWorkBook = excelApp.Workbooks.Add(misValue);
                   //foreach (DataTable table in finalDs.Tables)
                   //{
                   ////Add a new worksheet to workbook with the Datatable name
                   //Worksheet excelWorkSheet = excelWorkBook.Sheets.Add();
                   //excelWorkSheet.Name = table.TableName;

                   //for (int i = 1; i < table.Columns.Count + 1; i++)
                   //{
                   //    excelWorkSheet.Cells[1, i] = table.Columns[i - 1].ColumnName;
                   //}

                   //for (int j = 0; j < table.Rows.Count; j++)
                   //{
                   //    for (int k = 0; k < table.Columns.Count; k++)
                   //    {
                   //        excelWorkSheet.Cells[j + 2, k + 1] = table.Rows[j].ItemArray[k].ToString();
                   //    }
                   //}

                 //  object context;
                //   if (Request.Properties.TryGetValue("MS_HttpContext", out context))
                //   {
                //    //   var httpContext = context as HttpContextBase;
                    //   if (httpContext != null && httpContext.Session != null)
                    //   {
                           if (finalDs.Tables[0].TableName == "ProductFamily")
                           {
                               ExportTable = finalDs.Tables[3];
                           }
                           else
                           {
                               ExportTable = finalDs.Tables[0];
                           }
                     //  }
                  // }
                   //var objExport = new RKLib.ExportData.Export();
                   //objExport.ExportDetails(table, RKLib.ExportData.Export.ExportFormat.Excel, fileName);
                   // }
                   // excelWorkBook.SaveAs(fileName, XlFileFormat.xlWorkbookNormal, misValue, misValue, misValue, misValue, XlSaveAsAccessMode.xlExclusive, misValue, misValue, misValue, misValue, misValue);

                   //  excelWorkBook.Close();
                   //   excelApp.Quit();


                   //   DownLoad(fileName);


               }
               //  expData.CustomExport(finalDS, FileName.Trim(), Utilities.Export.ExportType.Excel);
           }
           catch (Exception objexception)
           {
               _logger.Info("Error in ExportDataSetToExcel");
               // Logger.Error("Error at Export : ExportXls", objexception);
               //return Request.CreateResponse(HttpStatusCode.InternalServerError);
           }
       }
       public void ExportDataSetToExcelSubProduct(DataSet finalDs, string fileName)
       {
           try
           {
               _logger.Info("ExportDataSetToExcelSubProduct");

               if (finalDs.Tables[0].Columns.Contains("FAMILY_ID_1"))
               {
                   finalDs.Tables[0].Columns.Remove("FAMILY_ID_1");
               }
               if (finalDs.Tables[0].Columns.Contains("FAMILY_FOOT_NOTES"))
               {
                   finalDs.Tables[0].Columns.Remove("FAMILY_FOOT_NOTES");
               }
               if (finalDs.Tables[0].Columns.Contains("FAMILY_STATUS"))
               {
                   finalDs.Tables[0].Columns.Remove("FAMILY_STATUS");
               }

               if (fileName.Trim() != "")
               {
                   //Creae an Excel application instance
                   // var excelApp = new Application();



                   // object misValue = System.Reflection.Missing.Value;

                   //Create an Excel workbook instance and open it from the predefined location

                   // Workbook excelWorkBook = excelApp.Workbooks.Add(misValue);
                   //foreach (DataTable table in finalDs.Tables)
                   //{
                   ////Add a new worksheet to workbook with the Datatable name
                   //Worksheet excelWorkSheet = excelWorkBook.Sheets.Add();
                   //excelWorkSheet.Name = table.TableName;

                   //for (int i = 1; i < table.Columns.Count + 1; i++)
                   //{
                   //    excelWorkSheet.Cells[1, i] = table.Columns[i - 1].ColumnName;
                   //}

                   //for (int j = 0; j < table.Rows.Count; j++)
                   //{
                   //    for (int k = 0; k < table.Columns.Count; k++)
                   //    {
                   //        excelWorkSheet.Cells[j + 2, k + 1] = table.Rows[j].ItemArray[k].ToString();
                   //    }
                   //}

                   object context;
                  //// if (Request.Properties.TryGetValue("MS_HttpContext", out context))
                  // {
                     //  var httpContext = context as HttpContextBase;
                     //  if (httpContext != null && httpContext.Session != null)
                     //  {
                           if (finalDs.Tables[0].TableName == "ProductFamily")
                           {
                               if (finalDs.Tables[4].Columns.Contains("SUBCATALOG_ITEM_NO"))
                               {
                                   finalDs.Tables[4].Columns["SUBCATALOG_ITEM_NO"].ColumnName = "SUBITEM#";
                               }
                               if (finalDs.Tables[4].Columns.Contains("CATALOG_ITEM_NO"))
                               {
                                   finalDs.Tables[4].Columns["CATALOG_ITEM_NO"].ColumnName = "ITEM#";
                               }
                               ExportTableSubProduct = finalDs.Tables[4];
                           }
                           else
                           {
                               if (finalDs.Tables.Count > 1)
                               {
                                   ExportTableSubProduct = finalDs.Tables[1];
                                   if (finalDs.Tables[1].Columns.Contains("SUBCATALOG_ITEM_NO"))
                                   {
                                       finalDs.Tables[1].Columns["SUBCATALOG_ITEM_NO"].ColumnName = "SUBITEM#";
                                   }
                                   if (finalDs.Tables[0].Columns.Contains("CATALOG_ITEM_NO"))
                                   {
                                       finalDs.Tables[0].Columns["CATALOG_ITEM_NO"].ColumnName = "ITEM#";
                                   }

                               }
                           }
                   //    }
               //    }
                   //var objExport = new RKLib.ExportData.Export();
                   //objExport.ExportDetails(table, RKLib.ExportData.Export.ExportFormat.Excel, fileName);
                   // }
                   // excelWorkBook.SaveAs(fileName, XlFileFormat.xlWorkbookNormal, misValue, misValue, misValue, misValue, XlSaveAsAccessMode.xlExclusive, misValue, misValue, misValue, misValue, misValue);

                   //  excelWorkBook.Close();
                   //   excelApp.Quit();


                   //   DownLoad(fileName);
               }
               //  expData.CustomExport(finalDS, FileName.Trim(), Utilities.Export.ExportType.Excel);
           }
           catch (Exception objexception)
           {
               _logger.Info("Error in ExportDataSetToExcelSubProduct");
               // Logger.Error("Error at Export : ExportXls", objexception);
               //return Request.CreateResponse(HttpStatusCode.InternalServerError);
           }
       }

       public void ExportDataSetToMsExcel(DataSet finalDs, string fileName)
       {
           _logger.Info("ExportDataSetToMsExcel");
           try
           {

               if (finalDs.Tables[0].Columns.Contains("FAMILY_ID_1"))
               {
                   finalDs.Tables[0].Columns.Remove("FAMILY_ID_1");
               }
               if (finalDs.Tables[0].Columns.Contains("FAMILY_FOOT_NOTES"))
               {
                   finalDs.Tables[0].Columns.Remove("FAMILY_FOOT_NOTES");
               }
               if (finalDs.Tables[0].Columns.Contains("FAMILY_STATUS"))
               {
                   finalDs.Tables[0].Columns.Remove("FAMILY_STATUS");
               }

               if (fileName.Trim() != "")
               {

                   //using Microsoft.Office.Interop.Excel;
                   //Creae an Excel application instance
                   var excelApp = new Microsoft.Office.Interop.Excel.Application();


                   string files = System.Reflection.Assembly.GetExecutingAssembly().Location;
                   files = files + "~/Content/" + fileName ;




                  // var files = Assembly.GetExecutingAssembly().Location("~/Content/") + fileName;
                   object misValue = Missing.Value;

                   // Create an Excel workbook instance and open it from the predefined location

                   Microsoft.Office.Interop.Excel.Workbook excelWorkBook = excelApp.Workbooks.Add(misValue);
                   foreach (DataTable table in finalDs.Tables)
                   {
                       //Add a new worksheet to workbook with the Datatable name
                       Microsoft.Office.Interop.Excel.Worksheet excelWorkSheet = excelWorkBook.Sheets.Add();
                       excelWorkSheet.Name = table.TableName;

                       for (int i = 1; i < table.Columns.Count + 1; i++)
                       {
                           excelWorkSheet.Cells[1, i] = table.Columns[i - 1].ColumnName;
                       }

                       for (int j = 0; j < table.Rows.Count; j++)
                       {
                           for (int k = 0; k < table.Columns.Count; k++)
                           {
                               excelWorkSheet.Cells[j + 2, k + 1] = table.Rows[j].ItemArray[k].ToString();
                           }
                       }

                       //object context;
                       //if (Request.Properties.TryGetValue("MS_HttpContext", out context))
                       //{
                       //    var httpContext = context as HttpContextBase;
                           //if (finalDs.Tables[0] != null)
                           //{
                               ExportTable = finalDs.Tables[0];

                         //  }
                      // }
                   }
                   //var objExport = new RKLib.ExportData.Export();
                   //objExport.ExportDetails(table, RKLib.ExportData.Export.ExportFormat.Excel, fileName);
                   // }
                   excelWorkBook.SaveAs(files, Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookNormal, misValue,
                       misValue, misValue, misValue, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlExclusive,
                       misValue, misValue, misValue, misValue, misValue);

                   excelWorkBook.Close();
                   excelApp.Quit();


                   //   DownLoad(fileName);
               }
               //  expData.CustomExport(finalDS, FileName.Trim(), Utilities.Export.ExportType.Excel);
           }
           catch (Exception objexception)
           {
               _logger.Info("Error in ExportDataSetToMsExcel");
               // Logger.Error("Error at Export : ExportXls", objexception);
               //return Request.CreateResponse(HttpStatusCode.InternalServerError);
           }
       }

       public void CreateTextFile(DataTable dt_DataTable, string strFilePath, string Delimeter, bool subproducts)
       {
           try
           {
               _logger.Info("CreateTextFile");
               // Create the Text file to which grid data will be exported.    

               if (Delimeter == "\\t")
               {
                   Delimeter = "\t";
               }
               else if (Delimeter == "\\s")
               {
                   Delimeter = "  ";
               }
               if (!subproducts)
               {
                   StreamWriter sw = new StreamWriter(strFilePath, false);
                   //First we will write the headers.
                   int iColCount = dt_DataTable.Columns.Count;
                   for (int i = 0; i < iColCount; i++)
                   {
                       sw.Write(dt_DataTable.Columns[i]);
                       if (i < iColCount - 1)
                       {
                           sw.Write(Delimeter);
                       }
                   }
                   sw.Write(sw.NewLine);

                   // Now write all the rows.

                   foreach (DataRow dr in dt_DataTable.Rows)
                   {
                       for (int i = 0; i < iColCount; i++)
                       {
                           if (!Convert.IsDBNull(dr[i]))
                           {
                               sw.Write(dr[i].ToString());
                           }
                           if (i < iColCount - 1)
                           {
                               sw.Write(Delimeter);
                           }
                       }
                       sw.Write(sw.NewLine);
                   }
                   sw.Close();
               }
               if (subproducts)
               {
                   StreamWriter sw1 = new StreamWriter(strFilePath, true);
                   sw1.Write(sw1.NewLine + "------Sub Products------");
                   sw1.Write(sw1.NewLine);
                   sw1.Write(sw1.NewLine);
                   //First we will write the headers.
                   int iColCount1 = dt_DataTable.Columns.Count;
                   for (int i = 0; i < iColCount1; i++)
                   {
                       sw1.Write(dt_DataTable.Columns[i]);
                       if (i < iColCount1 - 1)
                       {
                           sw1.Write(Delimeter);
                       }
                   }
                   sw1.Write(sw1.NewLine);
                   // Now write all the rows.
                   foreach (DataRow dr in dt_DataTable.Rows)
                   {
                       for (int i = 0; i < iColCount1; i++)
                       {
                           if (!Convert.IsDBNull(dr[i]))
                           {
                               sw1.Write(dr[i].ToString());
                           }
                           if (i < iColCount1 - 1)
                           {
                               sw1.Write(Delimeter);
                           }
                       }
                       sw1.Write(sw1.NewLine);
                   }
                   sw1.Close();

               }
           }
           catch (Exception)
           {
                _logger.Info("Error in CreateTextFile");
              
           }
          

           //if (FTPdetails[0] != null && FTPdetails[1] != null && FTPdetails[2] != null)
           //{
           //    UploadFTP(strFilePath, FTPdetails);
           //}
       }
       public void CreateCSVFile(DataTable dt_DataTable, string strFilePath, bool subprod)
       {
           try
           {
                 _logger.Info("CreateCSVFile");
           if (!subprod)
           {
               // Create the CSV file to which grid data will be exported.
               StreamWriter sw = new StreamWriter(strFilePath, false);
               //First we will write the headers.
               int iColCount = dt_DataTable.Columns.Count;
               for (int i = 0; i < iColCount; i++)
               {
                   sw.Write(dt_DataTable.Columns[i]);
                   if (i < iColCount - 1)
                   {
                       sw.Write(",");
                   }
               }
               sw.Write(sw.NewLine);
               // Now write all the rows.
               foreach (DataRow dr in dt_DataTable.Rows)
               {
                   for (int i = 0; i < iColCount; i++)
                   {
                       if (!Convert.IsDBNull(dr[i]))
                       {
                           sw.Write(dr[i].ToString());
                       }
                       if (i < iColCount - 1)
                       {
                           sw.Write(",");
                       }
                   }
                   sw.Write(sw.NewLine);
               }
               sw.Close();
           }
           if (subprod)
           {
               StreamWriter sw1 = new StreamWriter(strFilePath, true);
               sw1.Write(sw1.NewLine + "------Sub Products------");
               sw1.Write(sw1.NewLine);
               sw1.Write(sw1.NewLine);

               // Create the CSV file to which grid data will be exported.

               //First we will write the headers.
               int iColCount1 = dt_DataTable.Columns.Count;
               for (int i = 0; i < iColCount1; i++)
               {
                   sw1.Write(dt_DataTable.Columns[i]);
                   if (i < iColCount1 - 1)
                   {
                       sw1.Write(",");
                   }
               }
               sw1.Write(sw1.NewLine);
               // Now write all the rows.
               foreach (DataRow dr in dt_DataTable.Rows)
               {
                   for (int i = 0; i < iColCount1; i++)
                   {
                       if (!Convert.IsDBNull(dr[i]))
                       {
                           sw1.Write(dr[i].ToString());
                       }
                       if (i < iColCount1 - 1)
                       {
                           sw1.Write(",");
                       }
                   }
                   sw1.Write(sw1.NewLine);
               }
               sw1.Close();



           }
           }
           catch (Exception ex)
           {
                _logger.Info("Error in CreateCSVFile");
              
           }
       }
       public void CreateXMLFile(DataTable dt, string strFilePath, bool EnableSubProduct, string checktags)
       {

           try
           {
               _logger.Info("CreateXMLFile");
                         // Create the XML file to which grid data will be exported.
           //if (!subproducts)
           if (!EnableSubProduct)
           {
               StreamWriter sw = new StreamWriter(strFilePath, false);
               MemoryStream str = new MemoryStream();
               dt.WriteXml(str, true);
               str.Seek(0, SeekOrigin.Begin);
               StreamReader sr = new StreamReader(str);
               string xmlstr;
               xmlstr = sr.ReadToEnd();

               if (checktags.ToUpper() == "TRUE")
               {
                   xmlstr = xmlstr.Replace("</NewDataSet>", "");
                   // xmlstr = xmlstr + "</ NewDataSet >";
               }

               sw.Write(xmlstr);
               sw.Close();
           }
           //if (subproducts)
           if (EnableSubProduct)
           {
               StreamWriter sw = new StreamWriter(strFilePath, true);
               MemoryStream str = new MemoryStream();
               dt.WriteXml(str, true);
               str.Seek(0, SeekOrigin.Begin);
               StreamReader sr = new StreamReader(str);
               string xmlstr;
               xmlstr = sr.ReadToEnd();
               xmlstr = xmlstr.Replace("<NewDataSet>", "");

               sw.Write(xmlstr);
               sw.Close();
           }

           }
           catch (Exception)
           {
               _logger.Info("Error in CreateXMLFile");
               throw;
           }

       }



       public void SaveDisplayAttribute(DataTable displayAttributeDetails, string templateName)
       {
           try
           {
               _logger.Info("SaveDisplayAttribute");
              // var templateDetails = _dbcontext.TB_TEMPLATE_DETAILS.Join(_dbcontext.TB_PROJECT, ttd => ttd.PROJECT_ID, tp => tp.PROJECT_ID, (ttd, tp) => new { ttd, tp }).Where(x => x.tp.PROJECT_NAME == templateName).Select(y => y.ttd.TEMPLATE_ID).FirstOrDefault();
               String Query = "select ttd.TEMPLATE_ID from TB_TEMPLATE_DETAILS ttd join TB_PROJECT tp on tp.PROJECT_ID = ttd.PROJECT_ID where tp.PROJECT_NAME = '" + templateName + "'";
               SqlCommand cmd = new SqlCommand(Query, sqlConnection);
               cmd.Connection = sqlConnection;
               sqlConnection.Open();
               var templateDetails = (int)cmd.ExecuteScalar();
               sqlConnection.Close();

                                               
               foreach (DataRow item in displayAttributeDetails.Rows)
               {
                   //string attributeName = Convert.ToString(item.ATTRIBUTE_NAME);
                   //string DISPLAY_NAME = Convert.ToString(item.DISPLAY_NAME);


                   String Query1 = "  select DISPLAY_ID from TB_DISPLAY_ATTRIBUTENAME where TEMPLATE_ID = '" + templateDetails + "' and ATTRIBUTE_ID = '" + item["ATTRIBUTE_ID"] + "' and  ATTRIBUTE_NAME = '" + item["ATTRIBUTE_NAME"].ToString() + "' and DISPLAY_NAME = '" + item["DISPLAY_NAME"].ToString() + "' and ATTRIBUTE_TYPE = '" + item["ATTRIBUTE_TYPE"] + "'";
                   SqlCommand cmd1 = new SqlCommand(Query1, sqlConnection);
                   cmd1.Connection = sqlConnection;
                   sqlConnection.Open();
                   var checkExists = Convert.ToString(cmd1.ExecuteScalar());
                   sqlConnection.Close();

                   // var checkExists = _dbcontext.TB_DISPLAY_ATTRIBUTENAME.Where(x => x.TEMPLATE_ID == templateDetails && x.ATTRIBUTE_ID == item.ATTRIBUTE_ID && x.ATTRIBUTE_NAME == item.ATTRIBUTE_NAME.ToString() && x.DISPLAY_NAME == item.DISPLAY_NAME.ToString() && x.ATTRIBUTE_TYPE == item.ATTRIBUTE_TYPE);


                   if (!checkExists.Any())
                   {
                       //var displatAttribute = new TB_DISPLAY_ATTRIBUTENAME();
                       //displatAttribute.ATTRIBUTE_ID = item.ATTRIBUTE_ID;
                       //displatAttribute.TEMPLATE_ID = templateDetails;
                       //displatAttribute.ATTRIBUTE_NAME = item.ATTRIBUTE_NAME.ToString();
                       //displatAttribute.DISPLAY_NAME = item.DISPLAY_NAME.ToString();
                       //displatAttribute.ATTRIBUTE_TYPE = item.ATTRIBUTE_TYPE;
                       //_dbcontext.TB_DISPLAY_ATTRIBUTENAME.Add(displatAttribute);

                       string query = "INSERT INTO TB_DISPLAY_ATTRIBUTENAME (ATTRIBUTE_ID, TEMPLATE_ID, ATTRIBUTE_NAME, DISPLAY_NAME,ATTRIBUTE_TYPE)";
                       query += " VALUES ('" + item["ATTRIBUTE_ID"] + "','" + templateDetails + "','" + item["ATTRIBUTE_NAME"].ToString() + "','" + item["DISPLAY_NAME"].ToString() + "','" + item["ATTRIBUTE_TYPE"] + "')";

                       SqlCommand myCommand = new SqlCommand(query, sqlConnection);
                       // ... other parameters
                       sqlConnection.Open();
                       myCommand.ExecuteNonQuery();
                       sqlConnection.Close();

                   }
                   else
                   {
                       //var displatAttribute = _dbcontext.TB_DISPLAY_ATTRIBUTENAME.Where(x => x.TEMPLATE_ID == templateDetails && x.ATTRIBUTE_ID == item.ATTRIBUTE_ID && x.ATTRIBUTE_NAME == item.ATTRIBUTE_NAME.ToString() && x.DISPLAY_NAME == item.DISPLAY_NAME.ToString() && x.ATTRIBUTE_TYPE == item.ATTRIBUTE_TYPE).FirstOrDefault();
                       //displatAttribute.ATTRIBUTE_ID = item.ATTRIBUTE_ID;
                       //displatAttribute.TEMPLATE_ID = templateDetails;
                       //displatAttribute.ATTRIBUTE_NAME = item.ATTRIBUTE_NAME.ToString();
                       //displatAttribute.DISPLAY_NAME = item.DISPLAY_NAME.ToString();
                       //displatAttribute.ATTRIBUTE_TYPE = item.ATTRIBUTE_TYPE;

                       //  update TB_DISPLAY_ATTRIBUTENAME set ATTRIBUTE_ID = '" + item["ATTRIBUTE_ID"] + "' ,TEMPLATE_ID = '" + templateDetails + "', ATTRIBUTE_NAME =  '" + item["ATTRIBUTE_NAME"].ToString() + "', DISPLAY_NAME  = '" + item["DISPLAY_NAME"].ToString() + "' ,ATTRIBUTE_TYPE = '" + item["ATTRIBUTE_TYPE"] + "'   where DISPLAY_ID  = '"+checkExists+"'
                       string query = "update TB_DISPLAY_ATTRIBUTENAME set ATTRIBUTE_ID = '" + item["ATTRIBUTE_ID"] + "' ,TEMPLATE_ID = '" + templateDetails + "', ATTRIBUTE_NAME =  '" + item["ATTRIBUTE_NAME"].ToString() + "', DISPLAY_NAME  = '" + item["DISPLAY_NAME"].ToString() + "' ,ATTRIBUTE_TYPE = '" + item["ATTRIBUTE_TYPE"] + "'   where DISPLAY_ID  = '" + checkExists + "'";

                       SqlCommand myCommand = new SqlCommand(query, sqlConnection);
                       // ... other parameters

                       sqlConnection.Open();
                       myCommand.ExecuteNonQuery();
                       sqlConnection.Close();

                       //Try To update the Values.


                   }
               }
          
           }
           catch (Exception objexception)
           {
               _logger.Info("Error in SaveDisplayAttribute");
               // Logger.Error("Error at Export : SaveDisplayAttribute", objexception);
               throw;
               //return Request.CreateResponse(HttpStatusCode.InternalServerError);
           }

       }


        #endregion
    }
}
